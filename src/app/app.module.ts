import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from 'src/app/app.component';
import { CoreModule } from 'src/app/core/core.module';
import { States } from 'src/app/core/services/navigation.service';
import { AuthGuard } from 'src/app/core/guards/auth.guard';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    RouterModule.forRoot([
      { path: States.LandingPage, loadChildren: '../app/landing-page/landing-page.module#LandingPageModule', pathMatch: 'full' },
      { path: States.Access, loadChildren: '../app/access/access.module#AccessModule' },
      { path: States.Home, loadChildren: '../app/home/home.module#HomeModule', canActivate: [AuthGuard] },
      { path: States.NotFound, loadChildren: '../app/not-found/not-found.module#NotFoundModule' },
      { path: '**', redirectTo: States.NotFound },
    ]),
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
