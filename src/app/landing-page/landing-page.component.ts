import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { NavigationService } from 'src/app/core/services/navigation.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { Patterns } from 'src/app/shared/ui.utils';
import { ContactService } from 'src/app/core/services/contact.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: [
    './landing-page.component.scss',
    './css/default.css',
    './css/layout.css',
    './css/media-queries.css'
  ]
})
export class LandingPageComponent implements OnInit {
  public contactForm: FormGroup;
  public isLoading = false;
  public emailSent = false;

  public constructor(
    private navigationSvc: NavigationService,
    private fb: FormBuilder,
    private contactService: ContactService,
    private toastr: ToastService
  ) { }

  public ngOnInit(): void {
    this.createForm();
  }

  public goToSignup(): void {
    this.navigationSvc.signUp();
  }

  public goToPasswordRecovery(): void {
    this.navigationSvc.passwordRecovery();
  }

  public createForm(): void {
    this.contactForm = this.fb.group({
      from: ['', [Validators.required, Validators.pattern(Patterns.email)]],
      message: ['', Validators.required]
    });
  }

  public goToSignIn(): void {
    this.navigationSvc.signIn();
  }

  public goToSignUp(): void {
    this.navigationSvc.signUp();
  }

  public goToHome(): void {
    this.navigationSvc.home();
  }

  public sendEmail(): void {
    this.isLoading = true;
    this.contactService.sendContactEmail(this.contactForm.value)
      .subscribe(() => {
        this.emailSent = true;
        this.isLoading = false;
      },
        error => {
          this.toastr.handleError(error);
          this.isLoading = false;
        }
      );
  }
}
