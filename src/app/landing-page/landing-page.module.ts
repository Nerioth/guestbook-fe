import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { LandingPageComponent } from 'src/app/landing-page/landing-page.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  imports: [
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: LandingPageComponent
      }
    ])
  ],
  declarations: [LandingPageComponent]
})
export class LandingPageModule { }
