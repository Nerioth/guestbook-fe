import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { Patterns } from 'src/app/shared/ui.utils';
import { PendingUser } from 'src/app/shared/models/pending-user.model';
import { ToastService } from 'src/app/core/services/toastr.service';
import { NavigationService } from 'src/app/core/services/navigation.service';
import { CompanyService } from 'src/app/core/services/company.service';

@Component({
  selector: 'app-confirm-user',
  templateUrl: './confirm-user.component.html',
  styleUrls: ['./confirm-user.component.scss']
})

export class ConfirmUserComponent implements OnInit, OnDestroy {

  public user: PendingUser;
  public confirmForm: FormGroup;
  private sub: Subscription;
  private id: number;
  private confirmation: string;
  private accessPoint: string;
  public isLoading = false;
  public isAlive = true;

  public constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private toastr: ToastService,
    private navigationSvc: NavigationService,
    private companySvc: CompanyService
  ) { }

  public ngOnInit(): void {
    this.sub = this.route.params
      .pipe(
        takeWhile(() => this.isAlive))
      .subscribe(params => {
        this.id = params['id'];
        this.confirmation = params['confirmation'];
        this.accessPoint = `pendingusers/${this.id}/confirm?confirmationLink=${this.confirmation}`;
      });
    this.getUserInfo();
    this.createForm();
  }

  public ngOnDestroy(): void {
    this.isAlive = false;
  }

  public getUserInfo(): void {
    this.companySvc.getPendingUserInfo(this.accessPoint)
      .subscribe(user => this.user = user,
        error => this.toastr.handleError(error));
  }

  public createForm(): void {
    this.confirmForm = this.fb.group({
      name: ['',
        [
          Validators.pattern(Patterns.name)
        ]
      ],
      surname: ['',
        [
          Validators.pattern(Patterns.name)
        ]
      ],
      password: ['',
        [
          Validators.required,
        ]],
      passwordConfirmation: ['',
        [
          Validators.required
        ]
      ],
    });
  }

  public clearPasswordConfirmation(): void {
    this.confirmForm.get('passwordConfirmation').setValue('');
  }

  public confirmUser(): void {
    this.isLoading = true;
    this.companySvc.confirmUser(this.accessPoint, this.user, this.confirmForm.value, this.confirmForm.get('password').value)
      .subscribe(() => this.navigationSvc.home(),
        error => this.isLoading = false
      );
  }

}
