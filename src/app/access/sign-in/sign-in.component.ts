import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';

import { NavigationService } from 'src/app/core/services/navigation.service';
import { IdentityService } from 'src/app/core/services/identity.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { Patterns } from 'src/app/shared/ui.utils';
import { BlockService } from 'src/app/core/services/block.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit, OnDestroy {
  public signinForm: FormGroup;
  public isLoading = false;
  public inGuestMode = false;
  public isAlive = true;

  public constructor(
    private navigationSvc: NavigationService,
    private fb: FormBuilder,
    private identitySvc: IdentityService,
    private toastr: ToastService,
    private blockSvc: BlockService
    ) {}

  public ngOnInit(): void {
    this.isLoading = true;
    this.blockSvc.isBlocked()
    .pipe(
      takeWhile(() => this.isAlive))
      .subscribe(block => this.inGuestMode = block);
    this.identitySvc.isTokenValid().subscribe(isTokenValid => {
      if (isTokenValid && !this.inGuestMode) {
          this.navigationSvc.home();
      } else {
        this.isLoading = false;
      }
    });
    this.createForm();
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  public goToSignup(): void {
    this.navigationSvc.signUp();
  }

  public goToGuestbook(): void {
    this.navigationSvc.guestbook();
  }

  public goToPasswordRecovery(): void {
    this.navigationSvc.passwordRecovery();
  }

  public createForm(): void {
    this.signinForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(Patterns.email)]],
      password: ['', Validators.required]
    });
  }

  public signIn(): void {
    this.isLoading = true;
    this.identitySvc
      .login(this.signinForm.value.email, this.signinForm.value.password)
      .subscribe(
        () => {
          this.blockSvc.toggleBlock(false);
          this.navigationSvc.home();
          this.isLoading = false;
        },
        errorResponse => {
          this.isLoading = false;
          if (errorResponse.error.error_description) {
            this.toastr.localizedError('SIGNIN.INVALID-CREDENTIALS');
          } else {
            this.toastr.localizedError('SIGNIN.SERVER-NOT-RESPONDING');
          }
        }
      );
  }
}
