import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { AccessComponent } from 'src/app/access/access.component';
import { SignInComponent } from 'src/app/access/sign-in/sign-in.component';
import { SignUpComponent } from 'src/app/access/sign-up/sign-up.component';
import { States } from 'src/app/core/services/navigation.service';
import { PasswordRecoveryComponent } from 'src/app/access/password-recovery/password-recovery.component';
import { ConfirmCompanyComponent } from 'src/app/access/confirm-company/confirm-company.component';
import { NewPasswordComponent } from 'src/app/access/new-password/new-password.component';
import { TosDialogComponent } from 'src/app/access/sign-up/tos-dialog/tos-dialog.component';
import { ConfirmUserComponent } from 'src/app/access/confirm-user/confirm-user.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: AccessComponent,
        children: [
          {path: '', redirectTo: States.SignIn},
          {path: States.SignIn, component: SignInComponent},
          {path: States.SignUp, component: SignUpComponent},
          {path: States.PasswordRecovery, component: PasswordRecoveryComponent},
          {path: States.NewPassword, component: NewPasswordComponent},
          {path: States.ConfirmCompany, component: ConfirmCompanyComponent},
          {path: States.ConfirmUser, component: ConfirmUserComponent}
        ]
      }
    ]),
    ReactiveFormsModule
  ],
  declarations: [
    AccessComponent,
    SignInComponent,
    SignUpComponent,
    PasswordRecoveryComponent,
    ConfirmCompanyComponent,
    ConfirmUserComponent,
    NewPasswordComponent,
    TosDialogComponent
  ],
  entryComponents: [TosDialogComponent]
})
export class AccessModule { }
