import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { NavigationService } from 'src/app/core/services/navigation.service';
import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { Patterns } from 'src/app/shared/ui.utils';
import { TosDialogComponent } from 'src/app/access/sign-up/tos-dialog/tos-dialog.component';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  public signupForm: FormGroup;
  public showConfirmationMessage = false;
  public isLoading = false;

  public constructor(
    private navigationSvc: NavigationService,
    private fb: FormBuilder,
    private companySvc: CompanyService,
    private toastr: ToastService,
    public dialog: MatDialog,
  ) { }

  public ngOnInit(): void {
    this.createForm();
  }

  public goToSignIn(): void {
    this.navigationSvc.signIn();
  }

  public createForm(): void {
    this.signupForm = this.fb.group({
      name: ['',
        [
          Validators.required,
        ]
      ],
      vat: ['',
        [
          Validators.required,
        ]
      ],
      referentUserName: ['',
        [
          Validators.required,
          Validators.pattern(Patterns.email)
        ]],
      tos: ['',
        [
          Validators.required
        ]
      ],
    });
  }

  public signUp(): void {
    this.isLoading = true;
    this.companySvc.registerCompany(this.signupForm.value)
      .subscribe(() => {
        this.showConfirmationMessage = true;
        this.isLoading = false;
      },
        error => {
          this.toastr.handleError(error);
          this.isLoading = false;
        }
      );
  }

  public openTosDialog(): void {
    const dialogRef = this.dialog.open(TosDialogComponent, {
      maxWidth: '800px',
      maxHeight: '800px',
    });
  }

}
