import { Router } from '@angular/router';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-tos-dialog',
  templateUrl: './tos-dialog.component.html',
  styleUrls: ['./tos-dialog.component.scss']
})
export class TosDialogComponent implements OnInit {

  public constructor(
    public dialogRef: MatDialogRef<TosDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    }

  public ngOnInit(): void {
  }

}
