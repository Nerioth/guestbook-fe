import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { NavigationService } from 'src/app/core/services/navigation.service';
import { UserService } from 'src/app/core/services/user.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { Patterns } from 'src/app/shared/ui.utils';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.scss']
})
export class PasswordRecoveryComponent implements OnInit {

  public passwordRecoveryForm: FormGroup;
  public showConfirmationMessage = false;
  public isLoading = false;

  public constructor(
    private navigationSvc: NavigationService,
    private fb: FormBuilder,
    private userSvc: UserService,
    private toastr: ToastService
  ) { }

  public ngOnInit(): void {
    this.createForm();
  }

  public goToSignup(): void {
    this.navigationSvc.signUp();
  }

  public goToPasswordRecovery(): void {
    this.navigationSvc.passwordRecovery();
  }

  public createForm(): void {
    this.passwordRecoveryForm = this.fb.group({
      username: [
        '',
        [
          Validators.required,
          Validators.pattern(Patterns.email)
        ]
      ],
    });
  }

  public requestRecovery(): void {
    this.isLoading = true;
    this.userSvc.requestRecovery(this.passwordRecoveryForm.value)
      .subscribe(() => {
        this.showConfirmationMessage = true;
        this.isLoading = false;
      },
        error => this.toastr.handleError(error));
  }

  public goToSignIn(): void {
    this.navigationSvc.signIn();
  }

}
