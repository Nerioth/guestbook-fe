import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { User } from 'src/app/shared/models/user.model';
import { UserService } from 'src/app/core/services/user.service';
import { NavigationService } from 'src/app/core/services/navigation.service';
import { ToastService } from 'src/app/core/services/toastr.service';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.scss']
})
export class NewPasswordComponent implements OnInit {

  public user: User;
  public newPasswordForm: FormGroup;
  private sub: Subscription;
  private id: number;
  private confirmation: string;
  private accessPoint: string;
  public isLoading = false;

  constructor(
    private route: ActivatedRoute,
    private userSvc: UserService,
    private toastr: ToastService,
    private navigationSvc: NavigationService,
    private fb: FormBuilder) { }

  public ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.confirmation = params['confirmation'];
      this.accessPoint = `users/${this.id}/newpassword?confirmationLink=${this.confirmation}`;
    });
    this.getUserInfo();
    this.createForm();
  }

  public getUserInfo() {
    this.userSvc.getUserInfo(this.accessPoint)
      .subscribe(user => this.user = user,
        error => this.toastr.handleError(error));
  }

  public createForm(): void {
    this.newPasswordForm = this.fb.group({
      password: ['',
        [
          Validators.required,
        ]],
      passwordConfirmation: ['',
        [
          Validators.required
        ]
      ],
    });
  }

  public clearPasswordConfirmation(): void {
    this.newPasswordForm.get('passwordConfirmation').setValue('');
  }

  public setNewPassword(): void {
    this.isLoading = true;
    this.userSvc.setNewPassword(this.accessPoint, this.user.username, this.newPasswordForm.value)
      .subscribe(logged => {
        if (logged) {
          this.navigationSvc.home();
        }
      }, error => {
        this.toastr.handleError(error);
        this.isLoading = false;
      });
  }

}
