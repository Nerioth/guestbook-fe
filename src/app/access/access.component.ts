import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';



@Component({
  selector: 'app-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.scss'],
})

export class AccessComponent implements OnInit {

  public signinForm: FormGroup;

  public constructor() { }

  public ngOnInit(): void {
  }

}
