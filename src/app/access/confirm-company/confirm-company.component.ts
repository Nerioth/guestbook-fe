import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { takeWhile } from 'rxjs/operators';

import { PendingCompany } from 'src/app/shared/models/pending-company.model';
import { ToastService } from 'src/app/core/services/toastr.service';
import { NavigationService } from 'src/app/core/services/navigation.service';
import { CompanyService } from 'src/app/core/services/company.service';
import { Patterns } from 'src/app/shared/ui.utils';

@Component({
  selector: 'app-confirm-company',
  templateUrl: './confirm-company.component.html',
  styleUrls: ['./confirm-company.component.scss']
})
export class ConfirmCompanyComponent implements OnInit, OnDestroy {

  public company: PendingCompany;
  public confirmForm: FormGroup;
  private sub: Subscription;
  private id: number;
  private confirmation: string;
  private accessPoint: string;
  public isLoading = false;
  public isAlive = true;

  public constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private toastr: ToastService,
    private navigationSvc: NavigationService,
    private companySvc: CompanyService
  ) { }

  public ngOnInit(): void {
    this.sub = this.route.params
      .pipe(
        takeWhile(() => this.isAlive))
      .subscribe(params => {
        this.id = params['id'];
        this.confirmation = params['confirmation'];
        this.accessPoint = `pendingcompanies/${this.id}/confirm?confirmationLink=${this.confirmation}`;
      });
    this.getCompanyInfo();
    this.createForm();
  }

  public ngOnDestroy(): void {
    this.isAlive = false;
  }

  public getCompanyInfo(): void {
    this.companySvc.getPendingCompanyInfo(this.accessPoint)
      .subscribe(company => this.company = company,
        error => this.toastr.handleError(error));
  }

  public createForm(): void {
    this.confirmForm = this.fb.group({
      name: ['',
        [
          Validators.required,
          Validators.pattern(Patterns.name)
        ]
      ],
      surname: ['',
        [
          Validators.required,
          Validators.pattern(Patterns.name)
        ]
      ],
      password: ['',
        [
          Validators.required,
        ]],
      passwordConfirmation: ['',
        [
          Validators.required
        ]
      ],
    });
  }

  public clearPasswordConfirmation(): void {
    this.confirmForm.get('passwordConfirmation').setValue('');
  }

  public confirmCompany(): void {
    this.isLoading = true;
    this.companySvc.confirmCompany(this.accessPoint, this.company, this.confirmForm.value, this.confirmForm.get('password').value)
      .subscribe(() => this.navigationSvc.home(),
        error => this.isLoading = false
      );
  }

}
