export interface Referent {
    id: number;
    name: string;
    surname: string;
    email: string;
    phoneNumber: string;
    companyId: number;
}
