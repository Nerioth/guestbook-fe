export interface Identity {
  username: string;
  role: number;
}
