export interface PendingUser {
    id: number;
    username: string;
    company: string;
    role: number;
}
