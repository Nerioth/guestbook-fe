export interface Language {
  locale: string;
  label: string;
  default: boolean;
}
