export interface GuestGridView {
    id: number;
    fullName: string;
    fromCompany: string;
    referentId: number;
    badgeNumber: string;
    licensePlate: string;
    enter: Date;
    exit: Date;
}
