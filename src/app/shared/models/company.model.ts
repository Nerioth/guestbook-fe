export interface Company {
    id: number;
    name: string;
    vat: string;
    phoneNumber: string;
    logo: string;
    role: number;
    referentUserId: number;
    referentName: string;
    pendingUsers: number;
    users: number;
    guests: number;
    blocked: boolean;
    autocompleteReferentsEnabled: boolean;
    exitGuestByReferentsEnabled: boolean;
}
