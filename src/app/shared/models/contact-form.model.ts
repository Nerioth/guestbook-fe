export interface ContactForm {
    from: string;
    message: string;
}
