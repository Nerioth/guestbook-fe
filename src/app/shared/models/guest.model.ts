export interface Guest {
    id: number;
    name: string;
    surname: string;
    fromCompany: string;
    referentId: number;
    badgeNumber: string;
    licensePlate: string;
    enter: Date;
    exit: Date;
}
