export interface PendingCompany {
    id: number;
    name: string;
    vat: string;
    referentUsername: string;
    dateAdded: Date;
    role: number;
}
