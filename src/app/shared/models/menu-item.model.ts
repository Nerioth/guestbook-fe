export interface MenuItem {
  icon: string;
  content: string;
  state: string;
}
