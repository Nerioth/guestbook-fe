export interface NewPasswordRequest {
    password: string;
    passwordConfirmation: string;
}
