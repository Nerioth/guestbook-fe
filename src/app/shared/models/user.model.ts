export interface User {
    id: number;
    username: string;
    name: string;
    surname: string;
    role: number;
    companyId: number;
}
