export interface ReferentGridView {
    id: number;
    fullName: string;
    email: string;
    phoneNumber: string;
    companyId: number;
}
