export interface GuestReport {
    title: string;
    name: string;
    surname: string;
    fromCompany: string;
    referentFullName: string;
    referentPhone: string;
    badgeNumber: string;
    licensePlate: string;
    enter: string;
    exit: string;
}
