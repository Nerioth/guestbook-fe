export interface UserGridView {
    id: number;
    username: string;
    fullName: string;
    role: string;
    companyId: number;
}
