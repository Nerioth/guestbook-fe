import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';
import { TranslateModule } from '@ngx-translate/core';

import { MaterialModule } from 'src/app/shared/modules/material.module';
import { LanguageChooserDialogComponent } from 'src/app/shared/modals/language-chooser-dialog/language-chooser-dialog.component';
import { ConfirmEqualValidatorDirective } from 'src/app/shared/directives/confirm-equal-validator.directive';



@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    TranslateModule,
    MaterialModule,
    DxDataGridModule
  ],
  declarations: [
    LanguageChooserDialogComponent,
    ConfirmEqualValidatorDirective
  ],
  entryComponents: [
    LanguageChooserDialogComponent
  ],
  exports: [
    FormsModule,
    CommonModule,
    TranslateModule,
    MaterialModule,
    ConfirmEqualValidatorDirective,
    DxDataGridModule
  ]
})
export class SharedModule { }
