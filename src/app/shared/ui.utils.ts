export enum Patterns {
    email = '^[_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{2,3})$',
    name = '^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$'
}

export enum Roles {
    guest = 1,
    secretary = 2,
    admin = 3,
    superAdmin = 4
}

export enum UserStates {
    pending = 'ACCOUNT.PENDING',
    confirmed = 'ACCOUNT.CONFIRMED'
}

export enum MenuItemRoutes {
    account = '/home/account',
    today = '/home/today',
    history = '/home/history',
    guestbook = '/home/guestbook',
    companies = '/home/companies'
}

export enum ActiveMenuItems {
    accout = 'account',
    today = 'today',
    history = 'history',
    guestbook = 'guestbook',
    companies = 'companies'
}

export enum ErrorType {
    NullDto = 0,
    InvalidPassword = 1,
    InvalidCompanyName = 2,
    InvalidEmail = 3,
    InvalidVAT = 4,
    InvalidSubjectTemplate = 5,
    InvalidBodyTemplate = 6,
    PendingCompanyNotFound = 7,
    DuplicateVAT = 8,
    DuplicateUsername = 9,
    UserNotFound = 10,
    CompanyNotFound = 11,
    InvalidReferentName = 12,
    InvalidUserRole = 13,
    NotInCompany = 14,
    UserIsReferent = 15,
    PendingUserNotFound = 16,
    BlockedCompany = 17,
    InvalidFileExtension = 18,
    FileTooHeavy = 19,
    InGuestMode = 20,
    ReferentNotFound = 21,
    GuestNotFound = 22
}

export enum ErrorUtil {
  translateString = 'ERROR-TYPE'
}
