import { TranslateService } from '@ngx-translate/core';
import { Component, ElementRef, OnInit } from '@angular/core';


@Component({
  selector: 'app-language-chooser-dialog.component',
  templateUrl: './language-chooser-dialog.component.html',
  styleUrls: ['./language-chooser-dialog.component.scss']
})
export class LanguageChooserDialogComponent implements OnInit {

  public languageOptions: any[];

  public constructor(
    private translateSvc: TranslateService,
    elementRef: ElementRef
  ) {
  }

  ngOnInit(): void {
  }

  public onModalInit(): void {
  }

  public onModalViewReady(): void {
  }

  public onModalDestroy(): void {
  }

  public languageSelected(value: string): void {
    this.translateSvc.use(value);
  }

}
