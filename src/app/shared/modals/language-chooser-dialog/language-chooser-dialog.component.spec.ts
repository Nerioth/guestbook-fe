import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageChooserDialogComponent } from 'src/app/shared/modals/language-chooser-dialog/language-chooser-dialog.component';
import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';



describe('EstimateEnableRequestDialogComponent', () => {
  let component: LanguageChooserDialogComponent;
  let fixture: ComponentFixture<LanguageChooserDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        SharedModule,
        RouterTestingModule
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(LanguageChooserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
