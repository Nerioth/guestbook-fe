import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { takeWhile } from 'rxjs/internal/operators/takeWhile';
import { MatDialog } from '@angular/material';
import { formatDate } from '@angular/common';

import { User } from 'src/app/shared/models/user.model';
import { IdentityService } from 'src/app/core/services/identity.service';
import { GuestGridView } from 'src/app/shared/models/guest-gridView.model';
import { ToastService } from 'src/app/core/services/toastr.service';
import { CompanyService } from 'src/app/core/services/company.service';
import { ErrorUtil, ErrorType } from 'src/app/shared/ui.utils';
import { FileService } from 'src/app/core/services/file.service';
import { ReferentDetailsDialogComponent } from 'src/app/home/today/referent-details-dialog/referent-details-dialog.component';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit, OnDestroy {
  public currentUser: User;
  public todayDate = new Date();
  public fromDate: Date;
  public toDate: Date;
  private isAlive = true;
  public guests: GuestGridView[];
  public guestCount = 0;

  public constructor(
    private dialog: MatDialog,
    private toastr: ToastService,
    private companySvc: CompanyService,
    private identitySvc: IdentityService,
    public translateSvc: TranslateService,
    private fileSvc: FileService
  ) { }

  public ngOnInit(): void {
    this.getCurrentUser();
  }

  public ngOnDestroy(): void {
    this.isAlive = false;
  }

  public fromDateChange(event: any): void {
    this.fromDate = event.value;
    this.toDate = null;
    this.getRangeGuests();
  }

  public toDateChange(event: any): void {
    this.toDate = event.value;
    this.getRangeGuests();
  }

  public exportToPdf(): void {
    if (this.fromDate) {
      const endDate = this.toDate ? this.toDate : this.fromDate;
      this.toastr.info(
        this.translateSvc.instant('GUEST-REPORT.GENERATING-PDF')
      );
      this.companySvc
        .getCompanyGuestsInRangePdfStream(
          this.currentUser.companyId,
          this.fromDate,
          endDate
        )
        .subscribe(stream => {
          this.toastr.success(
            this.translateSvc.instant('GUEST-REPORT.GENERATED-PDF')
          );
          const title = `${this.translateSvc.instant(
            'GUEST-REPORT.PDF-TITLE'
          )}_${formatDate(this.fromDate, 'dd-MM-yyyy', 'en')}_${formatDate(
            endDate,
            'dd-MM-yyyy',
            'en'
          )}`;
          this.fileSvc.downloadByBlob(title, 'application/pdf', stream);
        });
    }
  }

  private getRangeGuests() {
    if (this.fromDate) {
      const endDate = this.toDate ? this.toDate : this.fromDate;
      this.companySvc
        .getCompanyGuestsInRange(
          this.currentUser.companyId,
          this.fromDate,
          endDate
        )
        .subscribe(
          companyGuests => {
            this.guests = companyGuests.map(guest => {
              return <GuestGridView>{
                id: guest.id,
                fullName: `${guest.name ? guest.name : ''} ${
                  guest.surname ? guest.surname : ''
                  }`,
                fromCompany: guest.fromCompany,
                referentId: guest.referentId,
                badgeNumber: guest.badgeNumber,
                licensePlate: guest.licensePlate,
                enter: guest.enter,
                exit: guest.exit
              };
            });
            this.guestCount = companyGuests.length;
          },
          error =>
            this.toastr.localizedError(
              `${ErrorUtil.translateString}.${ErrorType[error.error.cause]}`
            )
        );
    }
  }

  private getCurrentUser(): void {
    this.identitySvc.currentIdentityEvt
      .pipe(takeWhile(() => this.isAlive))
      .subscribe((user: User) => (this.currentUser = user));
  }

  public onCellPrepared(evt: any): void {
    if (evt.rowType === 'header') {
      evt.cellElement.css('background-color', '#2196f3');
      evt.cellElement.css('color', 'white');
    }
  }

  public onToolbarPreparing(evt: any) {
    const toolbarItems = evt.toolbarOptions.items;

    toolbarItems.push({
      location: 'before',
      template: 'totalGroupCount'
    });
  }

  public openReferentDetailsDialog(referentId: number): void {
    this.companySvc.getCompanyReferentById(this.currentUser.companyId, referentId).subscribe(referent => {
      this.dialog.open(ReferentDetailsDialogComponent, {
        minWidth: '300px',
        maxWidth: '800px',
        maxHeight: '800px',
        data: { referent: referent }
      });
    }, error => this.toastr.handleError(error));
  }
}
