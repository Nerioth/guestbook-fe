import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { HistoryComponent } from 'src/app/home/history/history.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReferentDetailsDialogComponent } from 'src/app/home/today/referent-details-dialog/referent-details-dialog.component';
import { ReferentsDetailsDialogModule } from 'src/app/home/today/referent-details-dialog/referent-details-dialog.module';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: HistoryComponent
      }
    ]),
    ReactiveFormsModule,
    ReferentsDetailsDialogModule
  ],
  declarations: [
    HistoryComponent,
  ],
  entryComponents: [ReferentDetailsDialogComponent]
})
export class HistoryModule { }
