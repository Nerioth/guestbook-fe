import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-menu-icon-item',
  templateUrl: './menu-icon-item.component.html',
  styleUrls: ['./menu-icon-item.component.scss']
})
export class MenuIconItemComponent implements OnInit {
  @Input() public icon: string;
  @Input() public content: string;
  @Input() public isActive: string;

  constructor() { }

  public ngOnInit(): void {
  }
}
