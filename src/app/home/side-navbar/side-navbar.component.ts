import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { takeWhile, tap, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { MenuItem } from 'src/app/shared/models/menu-item.model';
import { IdentityService } from 'src/app/core/services/identity.service';
import { NavigationService, States } from 'src/app/core/services/navigation.service';
import { User } from 'src/app/shared/models/user.model';
import { MenuItemRoutes, Roles, ActiveMenuItems, ErrorType, ErrorUtil } from 'src/app/shared/ui.utils';
import { ToastService } from 'src/app/core/services/toastr.service';
import { CompanyService } from 'src/app/core/services/company.service';
import { Company } from 'src/app/shared/models/company.model';


@Component({
  selector: 'app-side-navbar',
  templateUrl: './side-navbar.component.html',
  styleUrls: ['./side-navbar.component.scss']
})
export class SideNavbarComponent implements OnInit, OnDestroy {

  public menuItems: MenuItem[] = [];
  public activeMenuItem: string;
  public isAlive = true;
  public currentUser: User;
  public company: Company;
  public fullName: string;
  public logoString = '';

  constructor(
    private router: Router,
    private identitySvc: IdentityService,
    private navigationSvc: NavigationService,
    private companySvc: CompanyService,
    private toastr: ToastService
  ) { }

  public ngOnInit(): void {
    this.setCurrentMenuItem();
    this.getCurrentUser();
    this.populateMenuItemsBasedOnRole(this.currentUser.role);
  }

  public ngOnDestroy(): void {
    this.isAlive = false;
  }

  private setCurrentMenuItem(): void {
    switch (this.router.url) {
      case MenuItemRoutes.account:
        this.activeMenuItem = ActiveMenuItems.accout;
        break;
      case MenuItemRoutes.today:
        this.activeMenuItem = ActiveMenuItems.today;
        break;
      case MenuItemRoutes.history:
        this.activeMenuItem = ActiveMenuItems.history;
        break;
      case MenuItemRoutes.guestbook:
        this.activeMenuItem = ActiveMenuItems.guestbook;
        break;
      case MenuItemRoutes.companies:
        this.activeMenuItem = ActiveMenuItems.companies;
        break;
    }
  }

  private populateMenuItemsBasedOnRole(role: Roles): void {
    switch (role) {
      case Roles.superAdmin:
        this.menuItems = [
          { icon: 'account', content: 'MENU-ITEMS.ACCOUNT', state: States.Account },
          { icon: 'calendar-today', content: 'MENU-ITEMS.TODAY', state: States.Today },
          { icon: 'history', content: 'MENU-ITEMS.HISTORY', state: States.History },
          { icon: 'book-open', content: 'MENU-ITEMS.GUESTBOOK', state: States.Guestbook },
          { icon: 'domain', content: 'MENU-ITEMS.COMPANIES', state: States.Companies }
        ];
        break;
      case Roles.admin:
        this.menuItems = [
          { icon: 'account', content: 'MENU-ITEMS.ACCOUNT', state: States.Account },
          { icon: 'calendar-today', content: 'MENU-ITEMS.TODAY', state: States.Today },
          { icon: 'history', content: 'MENU-ITEMS.HISTORY', state: States.History },
          { icon: 'book-open', content: 'MENU-ITEMS.GUESTBOOK', state: States.Guestbook },
        ];
        break;
      case Roles.secretary:
        this.menuItems = [
          { icon: 'calendar-today', content: 'MENU-ITEMS.TODAY', state: States.Today },
          { icon: 'history', content: 'MENU-ITEMS.HISTORY', state: States.History },
          { icon: 'book-open', content: 'MENU-ITEMS.GUESTBOOK', state: States.Guestbook },
        ];
        break;
    }
  }

  public activateMenuItem(state: string): void {
    this.activeMenuItem = state;
    this.goToSection(state);
  }

  private goToSection(state: string): void {
    this.router.navigate([`/home/${state}`]);
  }

  public getCurrentUser(): void {
    this.identitySvc.currentIdentityEvt.pipe(
      takeWhile(() => this.isAlive),
      tap((user: User) => {
        this.currentUser = user;
        this.fullName = user.name + ' ' + user.surname;
      }),
      switchMap(() => this.getCurrentCompany())
    )
      .subscribe(
        company => {
          this.company = company;
        },
        error => {
          if (error.error) {
            this.toastr.localizedError(`${ErrorUtil.translateString}.${ErrorType[error.error.cause]}`);
          } else {
            console.log(error.message);
          }
        }
      );
  }

  public getCurrentCompany(): Observable<Company> {
    this.companySvc.getBase64Logo(this.currentUser.companyId).subscribe(
      base64logo => {
        this.logoString = 'data:image/png;base64,' + base64logo.baseString;
      },
      error => this.toastr.handleError(error)
    );
    return this.companySvc.getCompanyInfo(`companies/${this.currentUser.companyId}`);
  }

  public logout(): void {
    this.isAlive = false;
    this.identitySvc.logout()
      .subscribe(
        () => this.navigationSvc.signIn(),
        error => this.toastr.handleError(error));
  }

}
