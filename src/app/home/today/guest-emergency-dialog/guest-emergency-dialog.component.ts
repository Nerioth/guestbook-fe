import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

import { ConfirmGuestExitDialogComponent } from 'src/app/home/today/confirm-guest-exit-dialog/confirm-guest-exit-dialog.component';
import { TodayGuestService } from 'src/app/core/services/today-guest.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { CompanyService } from 'src/app/core/services/company.service';
import { ReferentDetailsDialogComponent } from 'src/app/home/today/referent-details-dialog/referent-details-dialog.component';

@Component({
  selector: 'app-guest-emergency-dialog',
  templateUrl: './guest-emergency-dialog.component.html',
  styleUrls: ['./guest-emergency-dialog.component.scss']
})
export class GuestEmergencyDialogComponent implements OnInit {
  public date: Date;
  public isLoading = false;

  public constructor(
    public dialogRef: MatDialogRef<GuestEmergencyDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public todayGuestSvc: TodayGuestService,
    private companySvc: CompanyService,
    private translateSvc: TranslateService,
    private dialog: MatDialog,
    private toastr: ToastService
  ) {}

  public ngOnInit(): void {
    this.date = new Date();
  }

  public onCellPrepared(evt: any): void {
    if (evt.rowType === 'header') {
      evt.cellElement.css('background-color', '#2196f3');
      evt.cellElement.css('color', 'white');
    }
  }

  public onToolbarPreparing(evt: any) {
    const toolbarItems = evt.toolbarOptions.items;

    toolbarItems.push(
      {
        location: 'before',
        template: this.translateSvc.instant('TODAY.EMERGENCY-TITLE')
      }
    );
  }

  public openConfirmGuestExitDialog(guestId: number, guestName: string): void {
    this.dialog.open(ConfirmGuestExitDialogComponent, {
      maxWidth: '800px',
      maxHeight: '100%',
      data: {
        guestId: guestId,
        guestName: guestName
      }
    });
  }

  public openReferentDetailsDialog(referentId: number): void {
    this.companySvc.getCompanyReferentById(this.todayGuestSvc.currentUser.companyId, referentId).subscribe(referent => {
      this.dialog.open(ReferentDetailsDialogComponent, {
        minWidth: '300px',
        maxWidth: '800px',
        maxHeight: '800px',
        data: { referent: referent }
      });
    }, error => this.toastr.handleError(error));
  }
}
