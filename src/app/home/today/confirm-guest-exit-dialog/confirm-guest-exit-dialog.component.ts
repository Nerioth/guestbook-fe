import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup } from '@angular/forms';

import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { TodayComponent } from 'src/app/home/today/today.component';
import { TodayGuestService } from 'src/app/core/services/today-guest.service';


@Component({
  selector: 'app-confirm-guest-exit-dialog',
  templateUrl: './confirm-guest-exit-dialog.component.html',
  styleUrls: ['./confirm-guest-exit-dialog.component.scss']
})
export class ConfirmGuestExitDialogComponent implements OnInit {
  public addGuestForm: FormGroup;
  public isLoading = false;

  public component: TodayComponent;
  public guestId: number;
  public guestName: string;

  public constructor(
    public dialogRef: MatDialogRef<ConfirmGuestExitDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private companySvc: CompanyService,
    private toastr: ToastService,
    public todayGuestSvc: TodayGuestService
  ) {}

  public ngOnInit(): void {
    this.guestId = this.data.guestId;
    this.guestName = this.data.guestName;
  }

  public exitGuest(): void {
    this.isLoading = true;
    this.companySvc
      .exitCompanyGuest(this.todayGuestSvc.currentUser.companyId, this.guestId)
      .subscribe(
        () => {
          this.dialogRef.close(true);
          this.todayGuestSvc.updateData();
        },
        error => {
          this.toastr.handleError(error);
          this.isLoading = false;
        }
      );
  }
}
