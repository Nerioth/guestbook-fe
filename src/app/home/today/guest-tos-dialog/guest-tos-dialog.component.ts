import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-guest-tos-dialog',
  templateUrl: './guest-tos-dialog.component.html',
  styleUrls: ['./guest-tos-dialog.component.scss']
})
export class GuestTosDialogComponent implements OnInit {
  public constructor(
    public dialogRef: MatDialogRef<GuestTosDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  public ngOnInit(): void {}
}
