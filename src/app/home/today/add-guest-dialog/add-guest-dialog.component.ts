import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { startWith, map, tap, switchMap } from 'rxjs/operators';

import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { Patterns } from 'src/app/shared/ui.utils';
import { GuestTosDialogComponent } from 'src/app/home/today/guest-tos-dialog/guest-tos-dialog.component';
import { Referent } from 'src/app/shared/models/referent.model';
import { TodayGuestService } from 'src/app/core/services/today-guest.service';
import { Company } from 'src/app/shared/models/company.model';

@Component({
  selector: 'app-add-guest-dialog',
  templateUrl: './add-guest-dialog.component.html',
  styleUrls: ['./add-guest-dialog.component.scss']
})
export class AddGuestDialogComponent implements OnInit {
  public currentCompany: Company;
  public addGuestForm: FormGroup;
  public isLoading = false;
  referents: Referent[];
  filteredReferents: Observable<Referent[]>;

  public constructor(
    public dialogRef: MatDialogRef<AddGuestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private companySvc: CompanyService,
    private toastr: ToastService,
    private translateSvc: TranslateService,
    public dialog: MatDialog,
    public todayGuestSvc: TodayGuestService,
  ) { }

  public ngOnInit(): void {
    this.currentCompany = this.data.company;
    this.createForm();
    if (this.currentCompany.autocompleteReferentsEnabled) {
      this.getReferents();
    }
  }

  public getReferents(): void {
    this.companySvc.getCompanyReferents(this.todayGuestSvc.currentUser.companyId)
      .pipe(
        tap(
          companyReferents => {
            this.referents = companyReferents;
          }
        ),
        switchMap(() => this.companySvc.getDefaultReferent(this.todayGuestSvc.currentUser.companyId))
      )
      .subscribe(
        referent => {
          referent.name = this.translateSvc.instant('DEFAULT-REFERENT-OPTION');
          this.referents.push(referent);
          this.filteredReferents = this.addGuestForm.get('referentId').valueChanges
            .pipe(
              startWith(''),
              map(value => this._filterReferents(value))
            );
        },
        error => this.toastr.handleError(error)
      );
  }

  private _filterReferents(filteredName: any): Referent[] {
    if (filteredName.name || typeof filteredName !== 'string') {
      return this.referents;
    }
    const filterValue = filteredName.toLowerCase();
    return this.referents.filter(referent => referent.name.toLowerCase().includes(filterValue)
      || referent.surname.toLowerCase().includes(filterValue));
  }

  public showReferentName(referent: Referent): string {
    if (!referent) {
      return '';
    }
    return `${referent.name} ${referent.surname}`;
  }


  public createForm(): void {
    this.addGuestForm = this.fb.group({
      name: [
        '',
        [Validators.required, Validators.pattern(Patterns.name)]
      ],
      surname: [
        '',
        [Validators.required, Validators.pattern(Patterns.name)]
      ],
      fromCompany: [
        '',
        []
      ],
      referentId: [
        this.currentCompany.autocompleteReferentsEnabled ? '' : 1,
        [Validators.required]
      ],
      badgeNumber: [
        '',
        []
      ],
      licensePlate: [
        '',
        []
      ],
      tos: [
        '',
        [Validators.required]
      ],
      manualReferentName: [
        '',
        [Validators.required]
      ]
    });

    if (this.currentCompany.autocompleteReferentsEnabled) {
      this.addGuestForm.get('manualReferentName').clearValidators();
      this.addGuestForm.get('manualReferentName').updateValueAndValidity();
    } else {
      this.addGuestForm.get('referentId').clearValidators();
      this.addGuestForm.get('referentId').updateValueAndValidity();
    }
  }

  public addNewGuest(): void {
    this.isLoading = true;
    if (this.currentCompany.autocompleteReferentsEnabled) {
      this.addGuestAuto();
    } else {
      this.addGuestManual();
    }
  }

  private addGuestAuto(): void {
    this.addGuestForm.patchValue({ referentId: this.addGuestForm.get('referentId').value.id });
    this.companySvc.registarGuestAuto(this.addGuestForm.value, this.todayGuestSvc.currentUser.companyId).subscribe(
      () => {
        this.dialogRef.close(true);
        this.todayGuestSvc.updateData();
      },
      error => {
        this.toastr.handleError(error);
        this.isLoading = false;
      }
    );
  }

  private addGuestManual(): void {
    this.companySvc.registerGuestManual(this.addGuestForm.value, this.todayGuestSvc.currentUser.companyId).subscribe(
      () => {
        this.dialogRef.close(true);
        this.todayGuestSvc.updateData();
      },
      error => {
        this.toastr.handleError(error);
        this.isLoading = false;
      }
    );
  }

  public openTosDialog(): void {
    this.dialog.open(GuestTosDialogComponent, {
      maxWidth: '800px',
      maxHeight: '100%'
    });
  }
}
