import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

import { Referent } from 'src/app/shared/models/referent.model';

@Component({
  selector: 'app-referent-details-dialog',
  templateUrl: './referent-details-dialog.component.html',
  styleUrls: ['./referent-details-dialog.component.scss']
})

export class ReferentDetailsDialogComponent implements OnInit {
  public editReferentForm: FormGroup;
  private referent: Referent;

  public constructor(
    public dialogRef: MatDialogRef<ReferentDetailsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
  ) { }

  public ngOnInit(): void {
    this.referent = this.data.referent;
    this.createForm();
  }

  public createForm(): void {
    this.editReferentForm = this.fb.group({
      name: [this.referent.name],
      surname: [this.referent.surname],
      email: [this.referent.email ? this.referent.email : ''],
      phoneNumber: [this.referent.phoneNumber ? this.referent.phoneNumber : ''],
    });
    this.editReferentForm.disable();
  }
}
