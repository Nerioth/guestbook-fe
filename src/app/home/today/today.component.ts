import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { takeWhile } from 'rxjs/operators';
import { interval } from 'rxjs/internal/observable/interval';
import { TranslateService } from '@ngx-translate/core';

import { User } from 'src/app/shared/models/user.model';
import { IdentityService } from 'src/app/core/services/identity.service';
import { AddGuestDialogComponent } from 'src/app/home/today/add-guest-dialog/add-guest-dialog.component';
import { ConfirmGuestExitDialogComponent } from 'src/app/home/today/confirm-guest-exit-dialog/confirm-guest-exit-dialog.component';
import { GuestEmergencyDialogComponent } from 'src/app/home/today/guest-emergency-dialog/guest-emergency-dialog.component';
import { TodayGuestService } from 'src/app/core/services/today-guest.service';
import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { ReferentDetailsDialogComponent } from 'src/app/home/today/referent-details-dialog/referent-details-dialog.component';
import { Company } from 'src/app/shared/models/company.model';

@Component({
  selector: 'app-today',
  templateUrl: './today.component.html',
  styleUrls: ['./today.component.scss']
})
export class TodayComponent implements OnInit, OnDestroy {
  public currentUser: User;
  public currentCompany: Company;
  public date: Date;
  private isAlive = true;

  public constructor(
    private dialog: MatDialog,
    private identitySvc: IdentityService,
    public todayGuestSvc: TodayGuestService,
    private companySvc: CompanyService,
    private toastr: ToastService,
    public translateSvc: TranslateService
  ) { }

  public ngOnInit(): void {
    this.getCurrentUser();
    this.getCurrentCompany();
    this.setDate();
    this.todayGuestSvc.setCurrentUser(this.currentUser);
    this.todayGuestSvc.updateData();
    interval(1 * 60 * 1000)
      .pipe(takeWhile(() => this.isAlive))
      .subscribe(() => this.todayGuestSvc.updateData());
  }

  public ngOnDestroy(): void {
    this.isAlive = false;
  }

  private setDate(): void {
    this.date = new Date();
  }

  private getCurrentUser(): void {
    this.identitySvc.currentIdentityEvt
      .pipe(takeWhile(() => this.isAlive))
      .subscribe((user: User) => this.currentUser = user);
  }

  private getCurrentCompany(): void {
    this.companySvc.getCompanyById(this.currentUser.companyId)
      .subscribe(
        company => this.currentCompany = company,
        error => this.toastr.handleError(error)
      );
  }

  public onCellPrepared(evt: any): void {
    if (evt.rowType === 'header') {
      evt.cellElement.css('background-color', '#2196f3');
      evt.cellElement.css('color', 'white');
    }
  }

  public onToolbarPreparing(evt: any) {
    const toolbarItems = evt.toolbarOptions.items;

    toolbarItems.push(
      {
        location: 'before',
        template: this.translateSvc.instant('TODAY.GUESTS-TITLE'),
      },
    );
  }

  public openConfirmGuestExitDialog(guestId: number, guestName: string): void {
    this.dialog.open(ConfirmGuestExitDialogComponent, {
      maxWidth: '800px',
      maxHeight: '100%',
      data: {
        guestId: guestId,
        guestName: guestName
      }
    });
  }

  public openAddGuestDialog(): void {
    this.dialog.open(AddGuestDialogComponent, {
      maxWidth: '800px',
      maxHeight: '100%',
      data: {
        company: this.currentCompany,
      }
    });
  }

  public openGuestEmergencyDialog(): void {
    this.dialog.open(GuestEmergencyDialogComponent, {
      maxWidth: '1400px',
      maxHeight: '100%'
    });
  }

  public openReferentDetailsDialog(referentId: number): void {
    this.companySvc.getCompanyReferentById(this.currentUser.companyId, referentId).subscribe(referent => {
      this.dialog.open(ReferentDetailsDialogComponent, {
        minWidth: '300px',
        maxWidth: '800px',
        maxHeight: '800px',
        data: { referent: referent }
      });
    }, error => this.toastr.handleError(error));
  }
}
