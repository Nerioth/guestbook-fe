import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { TodayComponent } from 'src/app/home/today/today.component';
import { AddGuestDialogComponent } from 'src/app/home/today/add-guest-dialog/add-guest-dialog.component';
import { GuestEmergencyDialogComponent } from 'src/app/home/today/guest-emergency-dialog/guest-emergency-dialog.component';
import { ConfirmGuestExitDialogComponent } from 'src/app/home/today/confirm-guest-exit-dialog/confirm-guest-exit-dialog.component';
import { GuestTosDialogComponent } from 'src/app/home/today/guest-tos-dialog/guest-tos-dialog.component';
import { ReferentDetailsDialogComponent } from 'src/app/home/today/referent-details-dialog/referent-details-dialog.component';
import { ReferentsDetailsDialogModule } from 'src/app/home/today/referent-details-dialog/referent-details-dialog.module';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: TodayComponent
      }
    ]),
    ReactiveFormsModule,
    ReferentsDetailsDialogModule
  ],
  declarations: [
    TodayComponent,
    AddGuestDialogComponent,
    GuestEmergencyDialogComponent,
    ConfirmGuestExitDialogComponent,
    GuestTosDialogComponent,
  ],
  entryComponents: [
    AddGuestDialogComponent,
    GuestEmergencyDialogComponent,
    ConfirmGuestExitDialogComponent,
    GuestTosDialogComponent,
    ReferentDetailsDialogComponent
  ],
})
export class TodayModule {}
