import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from 'src/app/home/home.component';
import { States } from 'src/app/core/services/navigation.service';
import { ToolbarComponent } from 'src/app/home/toolbar/toolbar.component';
import { SideNavbarComponent } from 'src/app/home/side-navbar/side-navbar.component';
import { MenuIconItemComponent } from 'src/app/home/side-navbar/menu-icon-item/menu-icon-item.component';
import { SuperAdminAuthGuard } from 'src/app/core/guards/super-admin-auth.guard';
import { AdminAuthGuard } from 'src/app/core/guards/admin-auth.guard';
import { SecretaryAuthGuard } from 'src/app/core/guards/secretary-auth.guard';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { GuestAuthGuard } from 'src/app/core/guards/guest.guard';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomeComponent,
        children: [
          {
            path: '',
            redirectTo: States.Account,
            canActivate: [AuthGuard, AdminAuthGuard, GuestAuthGuard]
          },
          {
            path: States.Account,
            loadChildren: 'src/app/home/account/account.module#AccountModule',
            canActivate: [AuthGuard, AdminAuthGuard, GuestAuthGuard]
          },
          {
            path: States.Companies,
            loadChildren:
              'src/app/home/companies/companies.module#CompaniesModule',
            canActivate: [AuthGuard, SuperAdminAuthGuard, GuestAuthGuard]
          },
          {
            path: States.Guestbook,
            loadChildren:
              'src/app/home/guestbook/guestbook.module#GuestbookModule',
            canActivate: [AuthGuard, SecretaryAuthGuard],
            canDeactivate: [GuestAuthGuard]
          },
          {
            path: States.History,
            loadChildren: 'src/app/home/history/history.module#HistoryModule',
            canActivate: [AuthGuard, SecretaryAuthGuard, GuestAuthGuard]
          },
          {
            path: States.Today,
            loadChildren: 'src/app/home/today/today.module#TodayModule',
            canActivate: [AuthGuard, SecretaryAuthGuard, GuestAuthGuard]
          }
        ]
      }
    ])
  ],
  declarations: [
    HomeComponent,
    ToolbarComponent,
    SideNavbarComponent,
    MenuIconItemComponent
  ]
})
export class HomeModule {}
