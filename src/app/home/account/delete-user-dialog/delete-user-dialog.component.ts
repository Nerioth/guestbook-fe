import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';

import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { UserStates, ErrorType } from 'src/app/shared/ui.utils';

@Component({
  selector: 'app-delete-user-dialog',
  templateUrl: './delete-user-dialog.component.html',
  styleUrls: ['./delete-user-dialog.component.scss']
})
export class DeleteUserDialogComponent implements OnInit {

  public isLoading = false;

  public constructor(
    public dialogRef: MatDialogRef<DeleteUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private companySvc: CompanyService,
    private toastr: ToastService,
  ) { }

  public ngOnInit(): void {
  }

  public deleteUser(): void {
    this.isLoading = true;
    this.deleteUserOfType(this.data.state === UserStates.pending ?
      this.companySvc.deletePendingUser(this.data.companyId, this.data.userId) :
      this.companySvc.deleteConfirmedUser(this.data.companyId, this.data.userId));
  }

  public deleteUserOfType(userDeleteByType: Observable<void>): void {
    userDeleteByType
      .subscribe(
        () => this.dialogRef.close(true),
        error => {
          this.toastr.localizedError(`ERROR-TYPE.${ErrorType[error.error.cause]}`);
          this.isLoading = false;
        });
  }

}
