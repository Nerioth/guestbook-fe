import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { Patterns, Roles } from 'src/app/shared/ui.utils';

@Component({
  selector: 'app-add-user-dialog',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.scss']
})
export class AddUserDialogComponent implements OnInit {
  public addUserForm: FormGroup;
  public isLoading = false;

  private companyId: number;
  public roles: any[] = [];

  public constructor(
    public dialogRef: MatDialogRef<AddUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private companySvc: CompanyService,
    private toastr: ToastService
  ) { }

  public ngOnInit(): void {
    this.companyId = this.data.companyId;
    this.populateRoles();
    this.createForm();
  }

  public populateRoles(): void {
    this.roles = [
      { value: Roles.secretary, viewValue: 'ACCOUNT.ADD-USER.SECRETARY' },
      { value: Roles.admin, viewValue: 'ACCOUNT.ADD-USER.ADMIN' }
    ];
  }

  public createForm(): void {
    this.addUserForm = this.fb.group({
      username: ['',
        [
          Validators.required,
          Validators.pattern(Patterns.email)
        ]
      ],
      userRole: ['',
        [
          Validators.required,
        ]
      ],
    });
  }

  public addNewUser(): void {
    this.isLoading = true;
    this.companySvc.registerUser(this.addUserForm.value, this.companyId)
      .subscribe(
        () => this.dialogRef.close(true),
        error => {
          this.toastr.handleError(error);
          this.isLoading = false;
        });
  }

}
