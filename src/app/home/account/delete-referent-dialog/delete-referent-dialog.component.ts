import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';

import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { UserStates, ErrorType } from 'src/app/shared/ui.utils';

@Component({
  selector: 'app-delete-referent-dialog',
  templateUrl: './delete-referent-dialog.component.html',
  styleUrls: ['./delete-referent-dialog.component.scss']
})
export class DeleteReferentDialogComponent implements OnInit {

  public isLoading = false;

  public constructor(
    public dialogRef: MatDialogRef<DeleteReferentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private companySvc: CompanyService,
    private toastr: ToastService,
  ) { }

  public ngOnInit(): void {
  }

  public deleteReferent(): void {
    this.isLoading = true;
    this.companySvc.deleteReferent(this.data.companyId, this.data.referentId)
      .subscribe(
        () => this.dialogRef.close(true),
        error => {
          this.toastr.localizedError(`ERROR-TYPE.${ErrorType[error.error.cause]}`);
          this.isLoading = false;
        });
  }

}
