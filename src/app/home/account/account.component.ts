import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeWhile, tap, switchMap } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

import { UserGridView } from 'src/app/shared/models/user-gridView.model';
import { User } from 'src/app/shared/models/user.model';
import { Company } from 'src/app/shared/models/company.model';
import { CompanyService } from 'src/app/core/services/company.service';
import { IdentityService } from 'src/app/core/services/identity.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { ResetPasswordDialogComponent } from 'src/app/home/account/reset-password-dialog/reset-password-dialog.component';
import { AddUserDialogComponent } from 'src/app/home/account/add-user-dialog/add-user-dialog.component';
import { DeleteUserDialogComponent } from 'src/app/home/account/delete-user-dialog/delete-user-dialog.component';
import { Roles, UserStates } from 'src/app/shared/ui.utils';
import { FileService } from 'src/app/core/services/file.service';
import { ReferentGridView } from 'src/app/shared/models/referent-gridView.model';
import { AddReferentDialogComponent } from 'src/app/home/account/add-referent-dialog/add-referent-dialog.component';
import { DeleteReferentDialogComponent } from 'src/app/home/account/delete-referent-dialog/delete-referent-dialog.component';
import { EditReferentDialogComponent } from 'src/app/home/account/edit-referent-dialog/edit-referent-dialog.component';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit, OnDestroy {
  public users: UserGridView[];
  public referents: ReferentGridView[];
  public currentUser: User;
  public company: Company;
  public isAlive = true;
  public companyForm: FormGroup;
  public formChanged = false;
  public companyInfoIsLoading = false;
  public fileToUpload: File;
  public logoString = '';
  public autocompleteReferentsEnabled: boolean;

  public constructor(
    private companySvc: CompanyService,
    private identitySvc: IdentityService,
    private fileSvc: FileService,
    private toastr: ToastService,
    private fb: FormBuilder,
    private dialog: MatDialog,
    private translateSvc: TranslateService
  ) { }

  public ngOnInit(): void {
    this.getCurrentUser();
    this.getCompanyDetails();
    this.createForm();
    this.getReferents();
    this.getUsers();
  }

  public ngOnDestroy(): void {
    this.isAlive = false;
  }

  public getCurrentUser(): void {
    this.identitySvc.currentIdentityEvt
      .pipe(takeWhile(() => this.isAlive))
      .subscribe((user: User) => (this.currentUser = user));
  }

  public getCompanyDetails(): void {
    this.companySvc
      .getBase64Logo(this.currentUser.companyId)
      .pipe(
        tap(
          base64logo => this.logoString = 'data:image/png;base64,' + base64logo.baseString
        ),
        switchMap(() =>
          this.companySvc.getCompanyInfo(`companies/${this.currentUser.companyId}`)
        )
      )
      .subscribe(
        company => {
          this.company = company;
          this.autocompleteReferentsEnabled = company.autocompleteReferentsEnabled;
          this.fillForm();
        },
        error => this.toastr.handleError(error)
      );
  }

  public createForm(): void {
    this.companyForm = this.fb.group({
      name: ['', Validators.required],
      referentName: ['', Validators.required],
      phoneNumber: ['']
    });
  }

  private fillForm(): void {
    this.companyForm.patchValue({
      name: this.company.name,
      referentName: this.company.referentName,
      phoneNumber: this.company.phoneNumber
    });
    this.formValueChanges();
  }

  private formValueChanges(): void {
    if (this.fileToUpload) {
      this.formChanged = true;
    }
    this.companyForm.valueChanges.subscribe(() => {
      this.formChanged = true;
    });
  }

  public saveCompanyChanges(): void {
    this.companyInfoIsLoading = true;
    this.companySvc
      .updateCompany(this.companyForm.value, this.company.id)
      .subscribe(
        () => {
          this.formChanged = false;
          this.companyInfoIsLoading = false;
        },
        error => {
          this.toastr.handleError(error);
          this.companyInfoIsLoading = false;
        }
      );
    if (this.fileToUpload) {
      this.convertAndSendLogo();
    }
  }

  public convertAndSendLogo(): void {
    this.companySvc.uploadLogo(this.company.id, this.logoString).subscribe(
      () => {
        this.fileToUpload = undefined;
        this.identitySvc.isTokenValid().subscribe();
      },
      error => this.toastr.handleError(error)
    );
  }

  public discardChanges(): void {
    this.fillForm();
    this.fileToUpload = undefined;
    this.logoString = this.company.logo;
    this.formChanged = false;
  }

  public onCellPrepared(evt: any): void {
    if (evt.rowType === 'header') {
      evt.cellElement.css('background-color', '#2196f3');
      evt.cellElement.css('color', 'white');
    }
  }

  public onToolbarPreparing(evt: any) {
    const toolbarItems = evt.toolbarOptions.items;

    toolbarItems.push({
      location: 'before',
      template: this.translateSvc.instant('ACCOUNT.COMPANY-USERS')
    });
  }

  public onToolbarPreparingReferents(evt: any) {
    const toolbarItems = evt.toolbarOptions.items;

    toolbarItems.push({
      location: 'before',
      template: this.translateSvc.instant('ACCOUNT.COMPANY-REFERENTS')
    });

    toolbarItems.push({
      location: 'after',
      widget: 'dxButton',
      options: {
        icon: 'add',
        onClick: this.openAddReferentDialog.bind(this)
      }
    });
  }

  public getReferents(): void {
    this.referents = [];
    this.companySvc
      .getCompanyReferents(this.currentUser.companyId)
      .subscribe(referents => {
        const mappedReferents = referents.map(referent => {
          return <ReferentGridView>{
            id: referent.id,
            companyId: referent.companyId,
            fullName: `${referent.name ? referent.name : ''} ${
              referent.surname ? referent.surname : ''
              }`,
            email: referent.email,
            phoneNumber: referent.phoneNumber
          };
        });
        this.referents.push(...mappedReferents);
      });
  }

  public getUsers(): void {
    this.users = [];
    this.companySvc
      .getPendingUsers(this.currentUser.companyId)
      .pipe(
        tap(pendingUsers => {
          const mappedUsers = pendingUsers.map(user => {
            return <UserGridView>{
              id: user.id,
              companyId: user.companyId,
              fullName: `${user.name ? user.name : ''} ${
                user.surname ? user.surname : ''
                }`,
              role: this.getRoleName(user.role),
              username: user.username,
              state: this.translateSvc.instant(UserStates.pending),
              stateValue: 0
            };
          });
          this.users.push(...mappedUsers);
        }),
        switchMap(() =>
          this.companySvc.getConfirmedUsers(this.currentUser.companyId)
        )
      )
      .subscribe(
        confirmedUsers => {
          const mappedUsers = confirmedUsers.map(user => {
            return <UserGridView>{
              id: user.id,
              companyId: user.companyId,
              fullName: `${user.name ? user.name : ''} ${
                user.surname ? user.surname : ''
                }`,
              role: this.getRoleName(user.role),
              username: user.username,
              state: this.translateSvc.instant(UserStates.confirmed),
              stateValue: 1
            };
          });
          this.users.push(...mappedUsers);
        },
        error => this.toastr.handleError(error)
      );
  }

  public getRoleName(role: Roles): string {
    switch (role) {
      case Roles.guest:
        return this.translateSvc.instant('ACCOUNT.GUEST');
      case Roles.secretary:
        return this.translateSvc.instant('ACCOUNT.SECRETARY');
      case Roles.admin:
        return this.translateSvc.instant('ACCOUNT.ADMIN');
    }
  }

  public openResetPasswordDialog(username: string): void {
    this.dialog.open(ResetPasswordDialogComponent, {
      maxWidth: '800px',
      maxHeight: '800px',
      data: { username: username }
    });
  }

  public openAddReferentDialog(): void {
    this.dialog
      .open(AddReferentDialogComponent, {
        maxWidth: '800px',
        maxHeight: '800px',
        data: { companyId: this.currentUser.companyId }
      })
      .afterClosed()
      .subscribe((referentCreated: boolean) => {
        if (referentCreated) {
          this.getReferents();
        }
      });
  }

  public openAddUserDialog(): void {
    this.dialog
      .open(AddUserDialogComponent, {
        maxWidth: '800px',
        maxHeight: '800px',
        data: { companyId: this.currentUser.companyId }
      })
      .afterClosed()
      .subscribe((userCreated: boolean) => {
        if (userCreated) {
          this.getUsers();
        }
      });
  }

  public openEditReferentDialog(referentId: number): void {
    this.companySvc
      .getCompanyReferentById(this.currentUser.companyId, referentId)
      .subscribe(
        referent => {
          this.dialog
            .open(EditReferentDialogComponent, {
              maxWidth: '800px',
              maxHeight: '800px',
              data: {
                companyId: this.currentUser.companyId,
                referent: referent
              }
            })
            .afterClosed()
            .subscribe((referentEdited: boolean) => {
              if (referentEdited) {
                this.getReferents();
              }
            });
        },
        error => this.toastr.handleError(error)
      );
  }

  public openDeleteUserDialog(
    userId: number,
    username: string,
    state: number
  ): void {
    this.dialog
      .open(DeleteUserDialogComponent, {
        maxWidth: '800px',
        maxHeight: '800px',
        data: {
          userId: userId,
          username: username,
          state: state,
          companyId: this.currentUser.companyId
        }
      })
      .afterClosed()
      .subscribe((userDeleted: boolean) => {
        if (userDeleted) {
          this.getUsers();
        }
      });
  }

  public openDeleteReferentDialog(referentId: number, fullName: string): void {
    this.dialog
      .open(DeleteReferentDialogComponent, {
        maxWidth: '800px',
        maxHeight: '800px',
        data: {
          referentId: referentId,
          fullName: fullName,
          companyId: this.currentUser.companyId
        }
      })
      .afterClosed()
      .subscribe((referentDeleted: boolean) => {
        if (referentDeleted) {
          this.getReferents();
        }
      });
  }

  public uploadLogo(): void {
    this.fileSvc.uploadFile(false, '.jpg', '.png', '.jpeg').subscribe(files => {
      this.fileToUpload = files[0];
      this.getBase64LogoAndSet();
      this.formValueChanges();
    });
  }

  public getBase64LogoAndSet(): void {
    this.fileSvc.getBase64FromFile(this.fileToUpload).subscribe(base64 => {
      this.logoString = base64;
    });
  }

  public changeReferentMode(): void {
    this.companySvc.changeReferentMode(this.currentUser.companyId)
      .subscribe(
        () => {
          this.toastr.localizedSuccess('ACCOUNT.CHANGE-MODE-SUCCESS');
          this.autocompleteReferentsEnabled = !this.autocompleteReferentsEnabled;
        },
        error => this.toastr.handleError(error)
      );
  }

  public changeGuestMode(): void {
    this.companySvc.changeGuestMode(this.currentUser.companyId)
      .subscribe(
        () => this.toastr.localizedSuccess('ACCOUNT.CHANGE-MODE-SUCCESS'),
        error => this.toastr.handleError(error)
      );
  }
}
