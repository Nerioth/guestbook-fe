import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

import { UserService } from 'src/app/core/services/user.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { ErrorType } from 'src/app/shared/ui.utils';

@Component({
  selector: 'app-reset-password-dialog',
  templateUrl: './reset-password-dialog.component.html',
  styleUrls: ['./reset-password-dialog.component.scss']
})
export class ResetPasswordDialogComponent implements OnInit {
  public isLoading = false;

  public constructor(
    public dialogRef: MatDialogRef<ResetPasswordDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userSvc: UserService,
    private toastr: ToastService,
    private translateSvc: TranslateService
  ) { }

  public ngOnInit(): void {
  }

  public resetPassword(): void {
    this.isLoading = true;
    this.userSvc.resetPasswordOfUser(this.data)
      .subscribe(
        () => this.dialogRef.close(),
        error => {
          this.toastr.localizedError(`ERROR-TYPE.${ErrorType[error.error.cause]}`);
          this.isLoading = false;
        }
      );
  }

}
