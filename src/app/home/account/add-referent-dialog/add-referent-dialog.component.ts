import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { Patterns } from 'src/app/shared/ui.utils';

@Component({
  selector: 'app-add-referent-dialog',
  templateUrl: './add-referent-dialog.component.html',
  styleUrls: ['./add-referent-dialog.component.scss']
})
export class AddReferentDialogComponent implements OnInit {
  public addReferentForm: FormGroup;
  public isLoading = false;

  private companyId: number;

  public constructor(
    public dialogRef: MatDialogRef<AddReferentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private companySvc: CompanyService,
    private toastr: ToastService  ) { }

  public ngOnInit(): void {
    this.companyId = this.data.companyId;
    this.createForm();
  }

  public createForm(): void {
    this.addReferentForm = this.fb.group({
      name: ['',
        [
          Validators.required,
        ]
      ],
      surname: ['',
        [
          Validators.required,
        ]
      ],
      email: ['',
        [
          Validators.pattern(Patterns.email)
        ]
      ],
      phoneNumber: ['',
        []
      ],
    });
  }

  public addNewReferent(): void {
    this.isLoading = true;
    this.companySvc.registerReferent(this.addReferentForm.value, this.companyId)
      .subscribe(
        () => this.dialogRef.close(true),
        error => {
          this.toastr.handleError(error);
          this.isLoading = false;
        });
  }

}
