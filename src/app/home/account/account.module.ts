import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { AccountComponent } from 'src/app/home/account/account.component';
import { ResetPasswordDialogComponent } from 'src/app/home/account/reset-password-dialog/reset-password-dialog.component';
import { AddUserDialogComponent } from 'src/app/home/account/add-user-dialog/add-user-dialog.component';
import { DeleteUserDialogComponent } from 'src/app/home/account/delete-user-dialog/delete-user-dialog.component';
import { AddReferentDialogComponent } from 'src/app/home/account/add-referent-dialog/add-referent-dialog.component';
import { DeleteReferentDialogComponent } from 'src/app/home/account/delete-referent-dialog/delete-referent-dialog.component';
import { EditReferentDialogComponent } from 'src/app/home/account/edit-referent-dialog/edit-referent-dialog.component';


@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: AccountComponent
      }
    ]),
    ReactiveFormsModule
  ],
  declarations: [
    AccountComponent,
    ResetPasswordDialogComponent,
    AddUserDialogComponent,
    AddReferentDialogComponent,
    EditReferentDialogComponent,
    DeleteUserDialogComponent,
    DeleteReferentDialogComponent,
  ],
  entryComponents: [
    ResetPasswordDialogComponent,
    AddUserDialogComponent,
    AddReferentDialogComponent,
    EditReferentDialogComponent,
    DeleteUserDialogComponent,
    DeleteReferentDialogComponent,
  ]
})
export class AccountModule { }
