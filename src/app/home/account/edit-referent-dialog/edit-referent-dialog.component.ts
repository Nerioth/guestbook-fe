import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { Patterns } from 'src/app/shared/ui.utils';
import { Referent } from 'src/app/shared/models/referent.model';

@Component({
  selector: 'app-edit-referent-dialog',
  templateUrl: './edit-referent-dialog.component.html',
  styleUrls: ['./edit-referent-dialog.component.scss']
})
export class EditReferentDialogComponent implements OnInit {
  public editReferentForm: FormGroup;
  public isLoading = false;

  private companyId: number;
  private referent: Referent;

  public constructor(
    public dialogRef: MatDialogRef<EditReferentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private companySvc: CompanyService,
    private toastr: ToastService,
  ) { }

  public ngOnInit(): void {
    this.companyId = this.data.companyId;
    this.referent = this.data.referent;
    this.createForm();
  }

  public createForm(): void {
    this.editReferentForm = this.fb.group({
      name: [this.referent.name,
        [
          Validators.required,
        ]
      ],
      surname: [this.referent.surname,
        [
          Validators.required,
        ]
      ],
      email: [this.referent.email ? this.referent.email : '',
        [
          Validators.pattern(Patterns.email)
        ]
      ],
      phoneNumber: [this.referent.phoneNumber ? this.referent.phoneNumber : '',
        []
      ],
    });
  }

  public editReferent(): void {
    this.isLoading = true;
    this.companySvc.updateReferent(this.editReferentForm.value, this.companyId, this.referent.id)
      .subscribe(
        () => this.dialogRef.close(true),
        error => {
          this.toastr.handleError(error);
          this.isLoading = false;
        });
  }

}
