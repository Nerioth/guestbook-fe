import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { FormControl, Validators } from '@angular/forms';
import { startWith, map, tap, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { GuestGridView } from 'src/app/shared/models/guest-gridView.model';
import { IdentityService } from 'src/app/core/services/identity.service';
import { User } from 'src/app/shared/models/user.model';
import { ConfirmGuestExitDialogComponent } from 'src/app/home/guestbook/confirm-guest-exit-dialog/confirm-guest-exit-dialog.component';
import { Referent } from 'src/app/shared/models/referent.model';
import { CompanyService } from 'src/app/core/services/company.service';
import { Company } from 'src/app/shared/models/company.model';
import { ToastService } from 'src/app/core/services/toastr.service';
import { Patterns } from 'src/app/shared/ui.utils';

@Component({
  selector: 'app-exit-guest-dialog',
  templateUrl: './exit-guest-dialog.component.html',
  styleUrls: ['./exit-guest-dialog.component.scss']
})
export class ExitGuestDialogComponent implements OnInit {
  public guests: GuestGridView[];
  public referents: Referent[];
  public filteredOptions: Observable<Referent[]>;
  public myControl = new FormControl();
  public searchSurnameControl = new FormControl();
  public currentUser: User;
  public currentReferent: Referent;
  public currentCompany: Company;
  public currentSurname: string;

  public constructor(
    public dialogRef: MatDialogRef<ExitGuestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private translateSvc: TranslateService,
    private dialog: MatDialog,
    private companySvc: CompanyService,
    private toastr: ToastService,
    private identitySvc: IdentityService
  ) { }

  public ngOnInit(): void {
    this.currentCompany = this.data.company;
    this.getCurrentUser();
    if (this.currentCompany.exitGuestByReferentsEnabled) {
      this.getReferents();
    }
    this.searchSurnameControl.setValidators([Validators.required, Validators.pattern(Patterns.name)]);
    this.searchSurnameControl.updateValueAndValidity();
  }

  private getCurrentUser(): void {
    this.identitySvc.currentIdentityEvt.subscribe(
      (user: User) => (this.currentUser = user)
    );
  }

  private getReferents(): void {
    if (this.currentCompany.autocompleteReferentsEnabled) {
      this.getAutoReferents();
    } else {
      this.getManualReferents();
    }
  }

  private getAutoReferents(): void {
    this.companySvc.getCompanyReferents(this.currentUser.companyId)
    .pipe(
      tap(
        companyReferents => {
          this.referents = companyReferents;
        }
      ),
      switchMap(() => this.companySvc.getDefaultReferent(this.currentUser.companyId))
    )
    .subscribe(
      referent => {
        referent.name = this.translateSvc.instant('DEFAULT-REFERENT-OPTION');
        this.referents.push(referent);
        this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filterReferents(value))
          );
      },
      error => this.toastr.handleError(error)
    );
  }

  private getManualReferents(): void {
    this.companySvc.getCompanyManualReferents(this.currentUser.companyId)
      .subscribe(companyReferents => {
        this.referents = companyReferents;
        this.filteredOptions = this.myControl.valueChanges.pipe(
          startWith(''),
          map(value => this._filterReferents(value))
        );
      });
  }

  private _filterReferents(filteredName: any): Referent[] {
    if (filteredName.name || typeof filteredName !== 'string') {
      return this.referents;
    }
    const filterValue = filteredName.toLowerCase();
    return this.referents.filter(
      referent =>
        referent.name.toLowerCase().includes(filterValue) ||
        referent.surname.toLowerCase().includes(filterValue)
    );
  }

  public showReferentName(referent: Referent): string {
    if (!referent) {
      return '';
    }
    return `${referent.name} ${referent.surname}`;
  }

  public getGuests(referent: Referent): void {
    this.currentReferent = referent;
    this.companySvc
      .getCompanyGuestsByReferent(this.currentUser.companyId, referent.id)
      .subscribe(companyGuests => {
        this.guests = companyGuests.map(guest => {
          return <GuestGridView>{
            id: guest.id,
            fullName: `${guest.name ? guest.name : ''} ${
              guest.surname ? guest.surname : ''
              }`,
            fromCompany: guest.fromCompany,
            referentId: guest.referentId,
            badgeNumber: guest.badgeNumber,
            licensePlate: guest.licensePlate,
            enter: guest.enter,
            exit: guest.exit
          };
        });
      });
  }

  public onCellPrepared(evt: any): void {
    if (evt.rowType === 'header') {
      evt.cellElement.css('background-color', '#2196f3');
      evt.cellElement.css('color', 'white');
    }
  }

  public onToolbarPreparing(evt: any) {
    const toolbarItems = evt.toolbarOptions.items;

    toolbarItems.push(
      {
        location: 'before',
        template: this.translateSvc.instant('GUEST-MODE.EXIT-GUEST-TITLE')
      },
      {
        location: 'after',
        template: 'referentPicker'
      }
    );
  }

  public openConfirmGuestExitDialog(guestId: number, guestName: string): void {
    this.dialog
      .open(ConfirmGuestExitDialogComponent, {
        maxWidth: '800px',
        maxHeight: '100%',
        data: {
          guestId: guestId,
          guestName: guestName,
          currentUsercompanyId: this.currentUser.companyId
        }
      })
      .afterClosed()
      .subscribe((exitMode: boolean) => {
        if (exitMode) {
          if (this.currentCompany.exitGuestByReferentsEnabled) {
            this.getGuests(this.currentReferent);
          } else {
            this.searchBySurname();
          }
        }
      });
  }

  public searchBySurname(): void {
    this.currentSurname = this.searchSurnameControl.value;
    this.companySvc.searchGuestsBySurname(this.currentCompany.id, this.searchSurnameControl.value)
      .subscribe(companyGuests => {
        this.guests = companyGuests.map(guest => {
          return <GuestGridView>{
            id: guest.id,
            fullName: `${guest.name ? guest.name : ''} ${
              guest.surname ? guest.surname : ''
              }`,
            fromCompany: guest.fromCompany,
            referentId: guest.referentId,
            badgeNumber: guest.badgeNumber,
            licensePlate: guest.licensePlate,
            enter: guest.enter,
            exit: guest.exit
          };
        });
      }
      );
  }
}
