import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { User } from 'src/app/shared/models/user.model';
import { ToastService } from 'src/app/core/services/toastr.service';
import { IdentityService } from 'src/app/core/services/identity.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-exit-guest-mode-dialog',
  templateUrl: './exit-guest-mode-dialog.component.html',
  styleUrls: ['./exit-guest-mode-dialog.component.scss']
})
export class ExitGuestModeDialogComponent implements OnInit {
  public currentUser;
  public signinForm: FormGroup;
  public constructor(
    public userSvc: UserService,
    public toastr: ToastService,
    private fb: FormBuilder,
    private identitySvc: IdentityService,
    public dialogRef: MatDialogRef<ExitGuestModeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  public ngOnInit(): void {
    this.getCurrentUser();
    this.createForm();
  }

  public getCurrentUser(): void {
    this.identitySvc.currentIdentityEvt.subscribe(
      (user: User) => (this.currentUser = user)
    );
  }

  public createForm(): void {
    this.signinForm = this.fb.group({
      password: ['', Validators.required]
    });
  }

  public exitGuestMode(): void {
    this.identitySvc
      .login(this.currentUser.username, this.signinForm.value.password)
      .subscribe(
        () => {
          this.dialogRef.close(true);
        },
        () => {
          this.toastr.localizedError('SIGNIN.INVALID-CREDENTIALS');
        }
      );
  }
}
