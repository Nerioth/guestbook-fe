import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-enter-guest-mode-dialog',
  templateUrl: './enter-guest-mode-dialog.component.html',
  styleUrls: ['./enter-guest-mode-dialog.component.scss']
})
export class EnterGuestModeDialogComponent implements OnInit {
  public constructor(
    public dialogRef: MatDialogRef<EnterGuestModeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  public ngOnInit(): void {}

  public enterGuestMode(): void {
    this.dialogRef.close(true);
  }
}
