import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from 'src/app/shared/shared.module';
import { GuestbookComponent } from 'src/app/home/guestbook/guestbook.component';
import { EnterGuestModeDialogComponent } from 'src/app/home/guestbook/enter-guest-mode-dialog/enter-guest-mode-dialog.component';
import { ExitGuestModeDialogComponent } from 'src/app/home/guestbook/exit-guest-mode-dialog/exit-guest-mode-dialog.component';
import { EnterGuestDialogComponent } from 'src/app/home/guestbook/enter-guest-dialog/enter-guest-dialog.component';
import { ExitGuestDialogComponent } from 'src/app/home/guestbook/exit-guest-dialog/exit-guest-dialog.component';
import { ConfirmGuestExitDialogComponent } from 'src/app/home/guestbook/confirm-guest-exit-dialog/confirm-guest-exit-dialog.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: GuestbookComponent
      }
    ]),
    ReactiveFormsModule
  ],
  declarations: [
    GuestbookComponent,
    EnterGuestModeDialogComponent,
    ExitGuestModeDialogComponent,
    EnterGuestDialogComponent,
    ExitGuestDialogComponent,
    ConfirmGuestExitDialogComponent
  ],
  entryComponents: [
    EnterGuestModeDialogComponent,
    ExitGuestModeDialogComponent,
    EnterGuestDialogComponent,
    ExitGuestDialogComponent,
    ConfirmGuestExitDialogComponent
  ]
})
export class GuestbookModule {}
