import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup } from '@angular/forms';

import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { ErrorType, ErrorUtil } from 'src/app/shared/ui.utils';

@Component({
  selector: 'app-confirm-guest-exit-dialog',
  templateUrl: './confirm-guest-exit-dialog.component.html',
  styleUrls: ['./confirm-guest-exit-dialog.component.scss']
})
export class ConfirmGuestExitDialogComponent implements OnInit {
  public addGuestForm: FormGroup;
  public isLoading = false;
  public currentUsercompanyId: number;
  public guestId: number;
  public guestName: string;

  public constructor(
    public dialogRef: MatDialogRef<ConfirmGuestExitDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private companySvc: CompanyService,
    private toastr: ToastService
  ) {}

  public ngOnInit(): void {
    this.guestId = this.data.guestId;
    this.guestName = this.data.guestName;
    this.currentUsercompanyId = this.data.currentUsercompanyId;
  }

  public exitGuest(): void {
    this.isLoading = true;
    this.companySvc
      .exitCompanyGuest(this.currentUsercompanyId, this.guestId)
      .subscribe(
        () => {
          this.dialogRef.close(true);
        },
        error => {
          this.toastr.localizedError(
            `${ErrorUtil.translateString}.${ErrorType[error.error.cause]}`
          );
          this.isLoading = false;
        }
      );
    this.dialogRef.close(true);
  }
}
