import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { takeWhile, switchMap, tap } from 'rxjs/operators';

import { EnterGuestModeDialogComponent } from 'src/app/home/guestbook/enter-guest-mode-dialog/enter-guest-mode-dialog.component';
import { ExitGuestModeDialogComponent } from 'src/app/home/guestbook/exit-guest-mode-dialog/exit-guest-mode-dialog.component';
import { BlockService } from 'src/app/core/services/block.service';
import { ExitGuestDialogComponent } from 'src/app/home/guestbook/exit-guest-dialog/exit-guest-dialog.component';
import { EnterGuestDialogComponent } from 'src/app/home/guestbook/enter-guest-dialog/enter-guest-dialog.component';
import { Company } from 'src/app/shared/models/company.model';
import { User } from 'src/app/shared/models/user.model';
import { IdentityService } from 'src/app/core/services/identity.service';
import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';

@Component({
  selector: 'app-guestbook',
  templateUrl: './guestbook.component.html',
  styleUrls: ['./guestbook.component.scss']
})
export class GuestbookComponent implements OnInit, OnDestroy {
  private isAlive = true;
  public guestMode: boolean;
  public currentUser: User;
  public currentCompany: Company;
  public logoString = '';

  constructor(
    private dialog: MatDialog,
    private identitySvc: IdentityService,
    public blockSvc: BlockService,
    private toastr: ToastService,
    private companySvc: CompanyService
  ) { }

  public ngOnInit(): void {
    this.getBlockState();
    this.getCurrentUser();
    this.getCompanyDetails();
  }

  public ngOnDestroy(): void {
    this.isAlive = false;
  }

  private getCurrentUser(): void {
    this.identitySvc.currentIdentityEvt
      .pipe(takeWhile(() => this.isAlive))
      .subscribe((user: User) => this.currentUser = user);
  }

  private getBlockState(): void {
    this.blockSvc.isBlocked()
    .pipe(takeWhile(() => this.isAlive))
    .subscribe(state => (this.guestMode = state));
  }

  public getCompanyDetails(): void {
    this.companySvc
      .getBase64Logo(this.currentUser.companyId)
      .pipe(
        tap(
          base64logo => this.logoString = 'data:image/png;base64,' + base64logo.baseString
        ),
        switchMap(() =>
          this.companySvc.getCompanyInfo(`companies/${this.currentUser.companyId}`)
        )
      )
      .subscribe(
        company => {
          this.currentCompany = company;
        },
        error => this.toastr.handleError(error)
      );
  }

  public openEnterGuestModeDialog(): void {
    this.dialog
      .open(EnterGuestModeDialogComponent, {
        maxWidth: '800px',
        maxHeight: '800px'
      })
      .afterClosed()
      .subscribe((enterMode: boolean) => {
        if (enterMode) {
          this.blockSvc.toggleBlock(true);
        }
      });
  }

  public openExitGuestModeDialog(): void {
    this.dialog
      .open(ExitGuestModeDialogComponent, {
        maxWidth: '800px',
        maxHeight: '800px'
      })
      .afterClosed()
      .subscribe((exitMode: boolean) => {
        if (exitMode) {
          this.blockSvc.toggleBlock(false);
        }
      });
  }

  public openEnterGuestDialog(): void {
    this.dialog
      .open(EnterGuestDialogComponent, {
        maxWidth: '800px',
        maxHeight: '100%',
        data: {
          company: this.currentCompany,
        }
      });
  }

  public openExitGuestDialog(): void {
    this.dialog
      .open(ExitGuestDialogComponent, {
        width: '80%',
        height: '80%',
        data: {
          company: this.currentCompany,
        }
      });
  }
}
