import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map, tap, switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { TodayGuestService } from 'src/app/core/services/today-guest.service';
import { Patterns } from 'src/app/shared/ui.utils';
import { User } from 'src/app/shared/models/user.model';
import { IdentityService } from 'src/app/core/services/identity.service';
import { GuestTosDialogComponent } from 'src/app/home/today/guest-tos-dialog/guest-tos-dialog.component';
import { Referent } from 'src/app/shared/models/referent.model';
import { Company } from 'src/app/shared/models/company.model';

@Component({
  selector: 'app-enter-guest-dialog',
  templateUrl: './enter-guest-dialog.component.html',
  styleUrls: ['./enter-guest-dialog.component.scss']
})
export class EnterGuestDialogComponent implements OnInit {
  public currentCompany: Company;
  public enterGuestForm: FormGroup;
  public isLoading = false;
  referents: Referent[];
  filteredReferents: Observable<Referent[]>;
  public currentUser: User;

  public constructor(
    public dialogRef: MatDialogRef<EnterGuestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private companySvc: CompanyService,
    private toastr: ToastService,
    private translateSvc: TranslateService,
    public dialog: MatDialog,
    public todayGuestSvc: TodayGuestService,
    private identitySvc: IdentityService,
  ) { }

  public ngOnInit(): void {
    this.currentCompany = this.data.company;
    this.createForm();
    this.getCurrentUser();
    this.getReferents();
  }

  private getCurrentUser(): void {
    this.identitySvc.currentIdentityEvt
      .subscribe((user: User) => (this.currentUser = user));
  }

  public getReferents(): void {
    this.companySvc.getCompanyReferents(this.currentUser.companyId)
      .pipe(
        tap(
          companyReferents => {
            this.referents = companyReferents;
          }
        ),
        switchMap(() => this.companySvc.getDefaultReferent(this.currentUser.companyId))
      )
      .subscribe(
        referent => {
          referent.name = this.translateSvc.instant('DEFAULT-REFERENT-OPTION');
          this.referents.push(referent);
          this.filteredReferents = this.enterGuestForm.get('referentId').valueChanges
            .pipe(
              startWith(''),
              map(value => this._filterReferents(value))
            );
        },
        error => this.toastr.handleError(error)
      );
  }

  private _filterReferents(filteredName: any): Referent[] {
    if (filteredName.name || typeof filteredName !== 'string') {
      return this.referents;
    }
    const filterValue = filteredName.toLowerCase();
    return this.referents.filter(referent => referent.name.toLowerCase().includes(filterValue)
      || referent.surname.toLowerCase().includes(filterValue));
  }

  public showReferentName(referent: Referent): string {
    if (!referent) {
      return '';
    }
    return `${referent.name} ${referent.surname}`;
  }


  public createForm(): void {
    this.enterGuestForm = this.fb.group({
      name: [
        '',
        [Validators.required, Validators.pattern(Patterns.name)]
      ],
      surname: [
        '',
        [Validators.required, Validators.pattern(Patterns.name)]
      ],
      fromCompany: [
        '',
        []
      ],
      referentId: [
        this.currentCompany.autocompleteReferentsEnabled ? '' : 1,
        [Validators.required]
      ],
      badgeNumber: [
        '',
        []
      ],
      licensePlate: [
        '',
        []
      ],
      tos: [
        '',
        [Validators.required]
      ],
      manualReferentName: [
        '',
        [Validators.required]
      ]
    });

    if (this.currentCompany.autocompleteReferentsEnabled) {
      this.enterGuestForm.get('manualReferentName').clearValidators();
      this.enterGuestForm.get('manualReferentName').updateValueAndValidity();
    } else {
      this.enterGuestForm.get('referentId').clearValidators();
      this.enterGuestForm.get('referentId').updateValueAndValidity();
    }
  }

  public addNewGuest(): void {
    this.isLoading = true;
    if (this.currentCompany.autocompleteReferentsEnabled) {
      this.addGuestAuto();
    } else {
      this.addGuestManual();
    }
  }

  private addGuestAuto(): void {
    this.enterGuestForm.patchValue({ referentId: this.enterGuestForm.get('referentId').value.id });
    this.companySvc.registarGuestAuto(this.enterGuestForm.value, this.currentUser.companyId).subscribe(
      () => {
        const name = this.enterGuestForm.get('name').value;
        const surname = this.enterGuestForm.get('surname').value;
        const confirmationMsg = this.translateSvc.instant('GUEST-MODE.CONFIRMATION-MSG');

        this.toastr.success(`${confirmationMsg}: ${name} ${surname}`);
        this.dialogRef.close(true);
      },
      error => {
        this.toastr.handleError(error);
        this.isLoading = false;
      }
    );
  }

  private addGuestManual(): void {
    this.companySvc.registerGuestManual(this.enterGuestForm.value, this.currentUser.companyId).subscribe(
      () => {
        const name = this.enterGuestForm.get('name').value;
        const surname = this.enterGuestForm.get('surname').value;
        const confirmationMsg = this.translateSvc.instant('GUEST-MODE.CONFIRMATION-MSG');

        this.toastr.success(`${confirmationMsg}: ${name} ${surname}`);
        this.dialogRef.close(true);
      },
      error => {
        this.toastr.handleError(error);
        this.isLoading = false;
      }
    );
  }

  public openTosDialog(): void {
    this.dialog.open(GuestTosDialogComponent, {
      maxWidth: '800px',
      maxHeight: '100%'
    });
  }
}
