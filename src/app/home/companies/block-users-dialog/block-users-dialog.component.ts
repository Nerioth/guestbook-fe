import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';

import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';

@Component({
  selector: 'app-block-users-dialog',
  templateUrl: './block-users-dialog.component.html',
  styleUrls: ['./block-users-dialog.component.scss']
})
export class BlockUsersDialogComponent implements OnInit {
  public isLoading = false;

  public constructor(
    public dialogRef: MatDialogRef<BlockUsersDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private companySvc: CompanyService,
    private toastr: ToastService
  ) { }

  public ngOnInit(): void {
  }

  public changeStateOfCompany(): void {
    this.isLoading = true;
    const actionToPerform =
      this.data.blocked ?
        this.companySvc.unblockAllUsersOfCompany(this.data.companyId)
        : this.companySvc.blockAllUsersOfCompany(this.data.companyId);
    actionToPerform
      .pipe(
        finalize(() => this.isLoading = false)
      )
      .subscribe(
        () => {
          this.dialogRef.close(true);
        }, error => this.toastr.handleError(error));
  }

}
