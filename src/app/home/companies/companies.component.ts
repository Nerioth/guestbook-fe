import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { CompanyService } from 'src/app/core/services/company.service';
import { Company } from 'src/app/shared/models/company.model';
import { ToastService } from 'src/app/core/services/toastr.service';
import { ResetReferentDialogComponent } from 'src/app/home/companies/reset-referent-dialog/reset-referent-dialog.component';
import { BlockUsersDialogComponent } from 'src/app/home/companies/block-users-dialog/block-users-dialog.component';
import { DeleteCompanyDialogComponent } from 'src/app/home/companies/delete-company-dialog/delete-company-dialog.component';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {

  public companies: Company[];

  public constructor(
    private companySvc: CompanyService,
    private toastr: ToastService,
    private dialog: MatDialog
  ) { }

  public ngOnInit(): void {
    this.getCompanies();
  }

  public getCompanies(): void {
    this.companySvc.getAllCompanies()
      .subscribe(
        companies => {
          this.companies = companies;
        }, error => this.toastr.handleError(error));
  }

  public openResetReferentDialog(companyId, referentName): void {
    this.dialog.open(ResetReferentDialogComponent, {
      maxWidth: '800px',
      maxHeight: '800px',
      data: {
        companyId: companyId,
        referentName: referentName
      }
    });
  }

  public openBlockUsersDialog(companyId, companyName, blocked, index): void {
    this.dialog.open(BlockUsersDialogComponent, {
      maxWidth: '800px',
      maxHeight: '800px',
      data: {
        companyId: companyId,
        companyName: companyName,
        blocked: blocked,
      }
    }).afterClosed().subscribe(stateChanged => {
      if (stateChanged) {
        this.companies[index].blocked = !this.companies[index].blocked;
      }
    });
  }

  public openDeleteCompanyDialog(companyId, companyName, index): void {
    this.dialog.open(DeleteCompanyDialogComponent, {
      maxWidth: '800px',
      maxHeight: '800px',
      data: {
        companyId: companyId,
        companyName: companyName,
      }
    }).afterClosed().subscribe(deleted => {
      if (deleted) {
        this.companies.splice(index, 1);
      }
    });
  }

}
