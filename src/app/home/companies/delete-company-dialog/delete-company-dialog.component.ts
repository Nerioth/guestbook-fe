import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';

import { CompanyService } from 'src/app/core/services/company.service';
import { ToastService } from 'src/app/core/services/toastr.service';

@Component({
  selector: 'app-delete-company-dialog',
  templateUrl: './delete-company-dialog.component.html',
  styleUrls: ['./delete-company-dialog.component.scss']
})
export class DeleteCompanyDialogComponent implements OnInit {
  public isLoading = false;

  public constructor(
    public dialogRef: MatDialogRef<DeleteCompanyDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private companySvc: CompanyService,
    private toastr: ToastService
  ) { }

  public ngOnInit(): void {
  }

  public deleteCompany(): void {
    this.isLoading = true;
    this.companySvc.deleteCompany(this.data.companyId)
      .pipe(
        finalize(() => this.isLoading = false)
      )
      .subscribe(
        () => {
          this.dialogRef.close(true);
        }, error => this.toastr.handleError(error));
  }

}
