import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CompaniesComponent } from 'src/app/home/companies/companies.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ResetReferentDialogComponent } from 'src/app/home/companies/reset-referent-dialog/reset-referent-dialog.component';
import { BlockUsersDialogComponent } from 'src/app/home/companies/block-users-dialog/block-users-dialog.component';
import { DeleteCompanyDialogComponent } from 'src/app/home/companies/delete-company-dialog/delete-company-dialog.component';


@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: CompaniesComponent
      }
    ]),
  ],
  declarations: [
    CompaniesComponent,
    ResetReferentDialogComponent,
    BlockUsersDialogComponent,
    DeleteCompanyDialogComponent
  ],
  entryComponents: [
    ResetReferentDialogComponent,
    BlockUsersDialogComponent,
    DeleteCompanyDialogComponent
  ]
})
export class CompaniesModule { }
