import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';

import { ToastService } from 'src/app/core/services/toastr.service';
import { CompanyService } from 'src/app/core/services/company.service';

@Component({
  selector: 'app-reset-referent-dialog',
  templateUrl: './reset-referent-dialog.component.html',
  styleUrls: ['./reset-referent-dialog.component.scss']
})

export class ResetReferentDialogComponent implements OnInit {
  public isLoading = false;

  public constructor(
    public dialogRef: MatDialogRef<ResetReferentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private companySvc: CompanyService,
    private toastr: ToastService
  ) { }

  public ngOnInit(): void {
  }

  public resetPassword(): void {
    this.isLoading = true;
    this.companySvc.resetReferentPasswordOfCompany(this.data.companyId)
      .pipe(
        finalize(() => this.isLoading = false)
      )
      .subscribe(
        () => {
          this.dialogRef.close();
        }, error => this.toastr.handleError(error));
  }

}
