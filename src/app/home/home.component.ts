import { Component, OnInit, HostListener } from '@angular/core';

import { DeviceService } from 'src/app/core/services/device.service';
import { NavigationService } from 'src/app/core/services/navigation.service';
import { BlockService } from 'src/app/core/services/block.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public blocked = false;
  public showUserNav = false;

  @HostListener('window:resize', ['$event'])
  public onWindowResize(): void {
    this.shouldShowUserNav();
  }

  public constructor(
    private deviceSvc: DeviceService,
    private navigationSvc: NavigationService,
    private blockSvc: BlockService
  ) {}

  public ngOnInit(): void {
    this.shouldShowUserNav();
  }

  public shouldShowUserNav() {
    this.blocked = false;
    this.showUserNav = !this.deviceSvc.isSmDown();
    this.blockSvc.isBlocked().subscribe(state => {
      if (state) {
        this.blocked = true;
        this.showUserNav = false;
      } else {
        this.blocked = false;
        this.showUserNav = !this.deviceSvc.isSmDown();
      }
    });
  }

  public goToSignIn(): void {
    this.navigationSvc.signIn();
  }
}
