import { Component, OnInit } from '@angular/core';

import { NavigationService } from 'src/app/core/services/navigation.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  public constructor(private navigationSvc: NavigationService) { }

  public ngOnInit(): void {
  }

  public goToHome(): void {
    this.navigationSvc.home();
  }

}
