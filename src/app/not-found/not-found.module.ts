import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { NotFoundComponent } from 'src/app/not-found/not-found.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: NotFoundComponent
      }
    ])
  ],
  declarations: [NotFoundComponent]
})
export class NotFoundModule { }
