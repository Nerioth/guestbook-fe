import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

export enum States {
  LandingPage = '',
  Home = 'home',
  Access = 'access',
  SignIn = 'signin',
  SignUp = 'signup',
  ConfirmCompany = 'confirm-company/:id/:confirmation',
  ConfirmUser = 'confirm-user/:id/:confirmation',
  PasswordRecovery = 'password-recovery',
  NewPassword = 'new-password/:id/:confirmation',
  Account = 'account',
  Today = 'today',
  History = 'history',
  Guestbook = 'guestbook',
  Companies = 'companies',
  NotFound = 'not-found'
}

@Injectable()
export class NavigationService {

  public constructor(private router: Router) { }

  public landingPage(): Promise<boolean> {
    return this.router.navigate(['/']);
  }

  public signIn(): Promise<boolean> {
    return this.router.navigate([`${States.Access}/${States.SignIn}`]);
  }

  public signUp(): Promise<boolean> {
    return this.router.navigate([`${States.Access}/${States.SignUp}`]);
  }

  public passwordRecovery(): Promise<boolean> {
    return this.router.navigate([`${States.Access}/${States.PasswordRecovery}`]);
  }

  public home(): Promise<boolean> {
    return this.router.navigate([`/${States.Home}`]);
  }

  public account(): Promise<boolean> {
    return this.router.navigate([`/${States.Home}/${States.Account}`]);
  }

  public today(): Promise<boolean> {
    return this.router.navigate([`/${States.Home}/${States.Today}`]);
  }

  public guestbook(): Promise<boolean> {
    return this.router.navigate([`/${States.Home}/${States.Guestbook}`]);
  }

}
