import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseHttpService } from 'src/app/core/services/base-http.service';
import { ContactForm } from 'src/app/shared/models/contact-form.model';

@Injectable()
export class ContactService {

    public constructor(private http: BaseHttpService) { }

    public sendContactEmail(contactForm: ContactForm): Observable<void> {
        return this.http.post('contact', contactForm);
    }
}
