import { Injectable } from '@angular/core';

import { LocalStorageService } from 'src/app/core/services/local-storage.service';

@Injectable()
export class SettingsService {

  private static readonly langKey = 'lang';
  private static readonly tokenKey = 'tkn';
  private static readonly tokenTypeKey = 'tknty';
  private static readonly refreshTokenKey = 'rftkn';

  public constructor(
    private localStorageSvc: LocalStorageService
  ) { }

  public get language(): string {
    return this.localStorageSvc.get(SettingsService.langKey);
  }

  public set language(language: string) {
    this.localStorageSvc.set(SettingsService.langKey, language);
  }

  public get token(): string {
    return this.localStorageSvc.get(SettingsService.tokenKey);
  }

  public set token(token: string) {
    this.localStorageSvc.set(SettingsService.tokenKey, token);
  }

  public get tokenType(): string {
    return this.localStorageSvc.get(SettingsService.tokenTypeKey);
  }

  public set tokenType(tokenType: string) {
    this.localStorageSvc.set(SettingsService.tokenTypeKey, tokenType);
  }

  public get refreshToken(): string {
    return this.localStorageSvc.get(SettingsService.refreshTokenKey);
  }

  public set refreshToken(refreshToken: string) {
    this.localStorageSvc.set(SettingsService.refreshTokenKey, refreshToken);
  }

  public clear(): void {
    this.localStorageSvc.clear();
  }

}
