import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import { LocalStorageService } from 'src/app/core/services/local-storage.service';

@Injectable()
export class BlockService {
  private state = new BehaviorSubject<boolean>(false);

  public constructor(
    private localStorageSvc: LocalStorageService
  ) { }

  isBlocked(): Observable<boolean> {
    const blocked = this.localStorageSvc.get<boolean>('blocked');
    this.state.next(blocked);
    return this.state.asObservable();
  }

  toggleBlock(block: boolean): void {
    this.localStorageSvc.set('blocked', block);
    this.state.next(block);
  }
}
