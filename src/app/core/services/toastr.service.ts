import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { ErrorUtil, ErrorType } from 'src/app/shared/ui.utils';

@Injectable()
export class ToastService {

  public constructor(private translateSvc: TranslateService) {
    toastr.options.positionClass = 'toast-bottom-right';
    toastr.options.tapToDismiss = true;
    toastr.options.closeButton = true;
    toastr.options.hideDuration = 140;
  }

  public handleError(error: any) {
    if (error.error.cause) {
      this.localizedError(`${ErrorUtil.translateString}.${ErrorType[error.error.cause]}`);
    } else {
      console.log(error);
    }
  }

  public localizedError(localizationKey: string): void {
    this.localize(localizationKey, this.error);
  }

  public error(message: string): void {
    toastr.error(message);
  }

  public localizedSuccess(localizationKey: string): void {
    this.localize(localizationKey, this.success);
  }

  public success(message: string): void {
    toastr.success(message);
  }

  public localizedInfo(localizationKey: string): void {
    this.localize(localizationKey, this.info);
  }

  public info(message: string): void {
    toastr.info(message);
  }

  public localizedWarning(localizationKey: string): void {
    this.localize(localizationKey, this.warning);
  }

  public warning(message: string): void {
    toastr.warning(message);
  }

  public todo(): void {
    this.warning('TODO');
  }

  // Utils

  private localize(localizationKey: string, action: (message: string) => void): void {
    this.translateSvc
      .get(localizationKey)
      .subscribe(res => action(res));
  }

}
