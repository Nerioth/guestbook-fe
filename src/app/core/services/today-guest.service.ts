import { Injectable } from '@angular/core';
import { tap, switchMap } from 'rxjs/operators';

import { ErrorType, ErrorUtil } from 'src/app/shared/ui.utils';
import { CompanyService } from 'src/app/core/services/company.service';
import { User } from 'src/app/shared/models/user.model';
import { GuestGridView } from 'src/app/shared/models/guest-gridView.model';
import { ToastService } from 'src/app/core/services/toastr.service';

@Injectable()
export class TodayGuestService {
  public guests: GuestGridView[];
  public currentGuests: GuestGridView[];
  public currentUser: User;
  public guestsToday: number;
  public guestsVisiting: number;

  public constructor(
    private companySvc: CompanyService,
    private toastr: ToastService
  ) {}

  public setCurrentUser(user: User): void {
    this.currentUser = user;
  }

  public updateData(): void {
    this.companySvc
      .getCompanyCurrentGuests(this.currentUser.companyId)
      .pipe(
        tap(currentGuests => {
          this.currentGuests = currentGuests.map(guest => {
            return <GuestGridView>{
              id: guest.id,
              fullName: `${guest.name ? guest.name : ''} ${
                guest.surname ? guest.surname : ''
              }`,
              fromCompany: guest.fromCompany,
              referentId: guest.referentId,
              badgeNumber: guest.badgeNumber,
              licensePlate: guest.licensePlate,
              enter: guest.enter,
              exit: guest.exit
            };
          });
          this.guestsVisiting = this.currentGuests.length;
        }),
        switchMap(() =>
          this.companySvc.getCompanyTodayGuests(this.currentUser.companyId)
        )
      )
      .subscribe(
        companyGuests => {
          this.guests = companyGuests.map(guest => {
            return <GuestGridView>{
              id: guest.id,
              fullName: `${guest.name ? guest.name : ''} ${
                guest.surname ? guest.surname : ''
              }`,
              fromCompany: guest.fromCompany,
              referentId: guest.referentId,
              badgeNumber: guest.badgeNumber,
              licensePlate: guest.licensePlate,
              enter: guest.enter,
              exit: guest.exit
            };
          });
          this.guestsToday = this.guests.length;
        },
        error =>
          this.toastr.localizedError(
            `${ErrorUtil.translateString}.${ErrorType[error.error.cause]}`
          )
      );
  }
}
