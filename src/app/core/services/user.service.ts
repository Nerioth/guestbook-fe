import { Injectable } from '@angular/core';
import { switchMap } from 'rxjs/operators';

import { Observable } from 'rxjs';

import { User } from 'src/app/shared/models/user.model';
import { BaseHttpService } from 'src/app/core/services/base-http.service';
import { IdentityService } from 'src/app/core/services/identity.service';
import { UserToReset } from 'src/app/shared/models/new-user-password';
import { PasswordRecovery } from 'src/app/shared/models/password-recovery.model';
import { NewPasswordRequest } from 'src/app/shared/models/new-password-request.model';

@Injectable()
export class UserService {

    public constructor(
        private http: BaseHttpService,
        private identitySvc: IdentityService) { }

    public getUserInfo(accessPoint: string): Observable<User> {
        return this.http.get<User>(accessPoint);
    }

    public requestRecovery(newRecoveryRequest: PasswordRecovery): Observable<void> {
        return this.http.post('users/newpassword', newRecoveryRequest);
    }

    public setNewPassword(accessPoint: string, username: string, newPasswordRequest: NewPasswordRequest): Observable<boolean> {
        return this.http.put(accessPoint, newPasswordRequest)
            .pipe(
                switchMap(() => this.identitySvc.login(username, newPasswordRequest.password)
                ));
    }

    public resetPasswordOfUser(user: UserToReset): Observable<void> {
        return this.http.post('users/newpassword', user);
    }
}
