import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { delay, tap, map, catchError } from 'rxjs/operators';

import { HttpOptions, BaseHttpService } from 'src/app/core/services/base-http.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { AuthResponse } from 'src/app/shared/models/auth-response.model';
import { User } from 'src/app/shared/models/user.model';
import { ToastService } from 'src/app/core/services/toastr.service';

@Injectable()
export class IdentityService {

  public currentIdentityEvt = new BehaviorSubject<User>(null);

  public constructor(private settingsSvc: SettingsService, private httpSvc: BaseHttpService, private toastr: ToastService) { }

  public login(username: string, password: string): Observable<boolean> {
    const ops = <HttpOptions>{
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };

    const content = this.httpSvc.buildQueryParams(
      { key: 'username', value: username },
      { key: 'password', value: password },
      { key: 'grant_type', value: 'password' },
      { key: 'client_id', value: 'ro.client' },
      { key: 'client_secret', value: 'secret' }
    );

    return this.httpSvc
      .post<AuthResponse>(`${this.httpSvc.baseUrl}connect/token`, content, null, false, ops, false)
      .pipe(
        tap(res => {
          this.settingsSvc.token = res.access_token;
          this.settingsSvc.tokenType = res.token_type;
          this.settingsSvc.refreshToken = res.refresh_token;
        }),
        map(res => !!res)
      );
  }

  public recoverPassword(): Observable<void> {
    return of(null).pipe(delay(1000));
  }

  public logout(): Observable<void> {
    this.settingsSvc.clear();
    this.setCurrentIdentity(null);
    return of(null);
  }

  public isTokenValid(): Observable<boolean> {
    if (!this.settingsSvc.token || !this.settingsSvc.tokenType || !this.settingsSvc.refreshToken) {
      return of(false);
    }
    return this.httpSvc.get<User>('users/self')
      .pipe(
        tap(res => this.setCurrentIdentity(res)),
        catchError(error => {
          this.toastr.handleError(error);
          return of(false);
        }),
        map(res => !!res)
      );
  }

  private setCurrentIdentity(identity: User): void {
    this.currentIdentityEvt.next(identity);
  }

}
