import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, map, switchMap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { SettingsService } from 'src/app/core/services/settings.service';
import { AuthResponse } from 'src/app/shared/models/auth-response.model';

@Injectable()
export class BaseHttpService {
  public readonly baseUrl = environment.baseUrl;
  public readonly baseApiUrl = `${this.baseUrl}api/`;

  public readonly unauthorizedEvt = new EventEmitter<void>();

  public constructor(
    protected http: HttpClient,
    private settingsSvc: SettingsService
  ) {}

  public get<T>(
    url: string,
    queryParams: HttpParams = null,
    auth = true,
    options: HttpOptions = null,
    useAsRelative = true
  ): Observable<T> {
    const action = () =>
      this.http.get<T>(this.buildUrl(url, useAsRelative), <{}>(
        Object.assign(this.buildOptions(auth, queryParams), options)
      ));

    return action().pipe<T>(
      catchError(err => this.handleError(err, () => action()))
    );
  }

  public post<T>(
    url: string,
    data: any,
    queryParams: HttpParams = null,
    auth = true,
    options: HttpOptions = null,
    useAsRelative = true
  ): Observable<T> {
    const action = () =>
      this.http.post<T>(this.buildUrl(url, useAsRelative), data, <{}>(
        Object.assign(this.buildOptions(auth, queryParams), options)
      ));

    return action().pipe<T>(
      catchError(err => this.handleError(err, () => action()))
    );
  }

  public delete(
    url: string,
    queryParams: HttpParams = null,
    auth = true,
    options: HttpOptions = null,
    useAsRelative = true
  ): Observable<void> {
    const action = () =>
      this.http.delete<void>(this.buildUrl(url, useAsRelative), <{}>(
        Object.assign(this.buildOptions(auth, queryParams), options)
      ));

    return action().pipe<void>(
      catchError(err => this.handleError(err, () => action()))
    );
  }

  public put<T>(
    url: string,
    data: any,
    queryParams: HttpParams = null,
    auth = true,
    options: HttpOptions = null,
    useAsRelative = true
  ): Observable<T> {
    const action = () =>
      this.http.put<T>(this.buildUrl(url, useAsRelative), data, <{}>(
        Object.assign(this.buildOptions(auth, queryParams), options)
      ));

    return action().pipe<T>(
      catchError(err => this.handleError(err, () => action()))
    );
  }

  public getForDownload(
    url: string,
    queryParams: HttpParams = null,
    auth = true,
    useAsRelative = true
  ): Observable<Blob> {
    // forse va ritornato un any. Blob è un test
    const action = () => {
      const options = this.buildOptions(auth, queryParams);
      options.responseType = 'blob';
      return this.http.get<Blob>(this.buildUrl(url, useAsRelative), <{}>(
        options
      ));
    };

    return action().pipe<Blob>(
      catchError(err => this.handleError(err, () => action()))
    );
  }

  public getForDownloadPost(
    url: string,
    data: any,
    queryParams: HttpParams = null,
    auth = true,
    useAsRelative = true
  ): Observable<Blob> {
    // forse va ritornato un any. Blob è un test
    const action = () => {
      const options = this.buildOptions(auth, queryParams);
      options.responseType = 'blob';
      return this.http.post<Blob>(this.buildUrl(url, useAsRelative), data, <{}>(
        options
      ));
    };

    return action().pipe<Blob>(
      catchError(err => this.handleError(err, () => action()))
    );
  }

  private handleError<T>(err: any, action: () => Observable<T>): Observable<T> {
    console.error('---', err, '---');

    if (err.status !== 401) {
      return throwError(err);
    }

    return this.refreshToken().pipe(
      switchMap(() => action()),
      catchError(refreshError => {
        this.unauthorizedEvt.emit();
        return throwError(refreshError);
      })
    );
  }

  public refreshToken(): Observable<boolean> {
    const ops = <HttpOptions>{
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };

    const content = this.buildQueryParams(
      { key: 'grant_type', value: 'refresh_token' },
      { key: 'client_id', value: 'ro.client' },
      { key: 'client_secret', value: 'secret' },
      { key: 'refresh_token', value: this.settingsSvc.refreshToken }
    ).toString();

    return this.post<AuthResponse>(
      `${this.baseUrl}connect/token`,
      content,
      null,
      false,
      ops,
      false
    ).pipe(
      tap(res => {
        this.settingsSvc.token = res.access_token;
        this.settingsSvc.tokenType = res.token_type;
        this.settingsSvc.refreshToken = res.refresh_token;
      }),
      map(res => !!res)
    );
  }

  private buildOptions(auth: boolean, queryParams: HttpParams): HttpOptions {
    let headers = new HttpHeaders().append('Content-Type', 'application/json');

    if (auth) {
      headers = headers.append(
        'Authorization',
        `${this.settingsSvc.tokenType} ${this.settingsSvc.token}`
      );
    }

    return <HttpOptions>{
      headers: headers,
      params: queryParams
    };
  }

  private buildUrl(url: string, useAsRelative: boolean): string {
    if (useAsRelative) {
      return `${this.baseApiUrl}${url}`;
    }
    return url;
  }

  public buildQueryParams(...items: { key; value }[]): HttpParams {
    let params = new HttpParams();
    items.forEach(i => (params = params.append(i.key, i.value)));
    return params;
  }
}

export interface HttpOptions {
  headers?: HttpHeaders | { [header: string]: string | string[] };
  observe?: 'body';
  params?: HttpParams | { [param: string]: string | string[] };
  reportProgress?: boolean;
  responseType?: string;
  withCredentials?: boolean;
}
