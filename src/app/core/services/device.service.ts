import { Injectable } from '@angular/core';

@Injectable()
export class DeviceService {

  public isSmDown(): boolean {
    return window.innerWidth <= 767;
  }

  public isMdDown(): boolean {
    return window.innerWidth <= 991;
  }

  public isLgDown(): boolean {
    return window.innerWidth <= 1199;
  }

  public isXlDown(): boolean {
    return window.innerWidth <= 1199;
  }

}
