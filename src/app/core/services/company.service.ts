import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

import { PendingUser } from 'src/app/shared/models/pending-user.model';
import { BaseHttpService } from 'src/app/core/services/base-http.service';
import { IdentityService } from 'src/app/core/services/identity.service';
import { PendingCompany } from 'src/app/shared/models/pending-company.model';
import { Company } from 'src/app/shared/models/company.model';
import { User } from 'src/app/shared/models/user.model';
import { Image64 } from 'src/app/shared/models/image64.model';
import { Guest } from 'src/app/shared/models/guest.model';
import { Referent } from 'src/app/shared/models/referent.model';
import { GuestReport } from 'src/app/shared/models/guest-report.model';

@Injectable()
export class CompanyService {
  public constructor(
    private http: BaseHttpService,
    private identitySvc: IdentityService,
    private translateSvc: TranslateService
  ) { }


  public getCompanyById(companyId: number): Observable<Company> {
    return this.http.get<Company>(`companies/${companyId}`);
  }

  public getPendingCompanyInfo(accessPoint: string): Observable<PendingCompany> {
    return this.http.get<PendingCompany>(accessPoint);
  }

  public getPendingUserInfo(accessPoint: string): Observable<PendingUser> {
    return this.http.get<PendingUser>(accessPoint);
  }

  public getCompanyInfo(accessPoint: string): Observable<Company> {
    return this.http.get<Company>(accessPoint);
  }

  public registerCompany(newCompany: Company): Observable<void> {
    return this.http.post(`pendingcompanies`, newCompany);
  }

  public confirmCompany(
    accessPoint: string,
    company: PendingCompany,
    confirmedCompany: Company,
    password: string
  ): Observable<boolean> {
    return this.http
      .post(accessPoint, confirmedCompany)
      .pipe(
        switchMap(() =>
          this.identitySvc.login(company.referentUsername, password)
        )
      );
  }

  public confirmUser(
    accessPoint: string,
    user: PendingUser,
    confirmedUser: User,
    password: string
  ): Observable<boolean> {
    return this.http
      .post(accessPoint, confirmedUser)
      .pipe(switchMap(() => this.identitySvc.login(user.username, password)));
  }

  public updateCompany(updatedCompany: Company, companyId: number): Observable<void> {
    return this.http.put(`companies/${companyId}`, updatedCompany);
  }

  public getPendingUsers(companyId: number): Observable<User[]> {
    return this.http.get(`companies/${companyId}/pendingUsers`);
  }

  public getConfirmedUsers(companyId: number): Observable<User[]> {
    return this.http.get(`companies/${companyId}/users`);
  }

  public getCompanyGuests(companyId: number): Observable<Guest[]> {
    return this.http.get(`companies/${companyId}/guests`);
  }

  public getCompanyGuestsByReferent(id: number, referentId: number): Observable<Guest[]> {
    return this.http.get(`companies/${id}/referents/${referentId}/guests`);
  }

  public getCompanyGuestsInRange(
    companyId: number,
    from: Date,
    to: Date
  ): Observable<Guest[]> {
    return this.http.get(
      `companies/${companyId}/guests-in-range/${formatDate(
        from,
        'dd-MM-yyyy',
        'en'
      )}/${formatDate(to, 'dd-MM-yyyy', 'en')}`
    );
  }

  public getCompanyGuestsInRangePdfStream(
    companyId: number,
    from: Date,
    to: Date
  ): Observable<Blob> {
    const list = [
      this.translateSvc.instant('GUEST-REPORT.Title'),
      this.translateSvc.instant('GUEST-REPORT.Name'),
      this.translateSvc.instant('GUEST-REPORT.Surname'),
      this.translateSvc.instant('GUEST-REPORT.Enter'),
      this.translateSvc.instant('GUEST-REPORT.Exit'),
      this.translateSvc.instant('GUEST-REPORT.ReferentName'),
      this.translateSvc.instant('GUEST-REPORT.ReferentPhone'),
      this.translateSvc.instant('GUEST-REPORT.FromCompany'),
      this.translateSvc.instant('GUEST-REPORT.BadgeNumber'),
      this.translateSvc.instant('GUEST-REPORT.LicensePlate')
    ];

    const params = <GuestReport>{
      title:
        list[0] +
        `${formatDate(
          from,
          'longDate',
          this.translateSvc.currentLang
        )} - ${formatDate(to, 'longDate', this.translateSvc.currentLang)}`,
      name: list[1],
      surname: list[2],
      enter: list[3],
      exit: list[4],
      referentFullName: list[5],
      referentPhone: list[6],
      fromCompany: list[7],
      badgeNumber: list[8],
      licensePlate: list[9]
    };

    return this.http.getForDownloadPost(
      `companies/${companyId}/guests-in-range/
${formatDate(from, 'dd-MM-yyyy', 'en')}/
${formatDate(to, 'dd-MM-yyyy', 'en')}/
report`,
      params
    );
  }

  public getCompanyTodayGuests(companyId: number): Observable<Guest[]> {
    return this.http.get(`companies/${companyId}/guests/today`);
  }

  public getCompanyCurrentGuests(companyId: number): Observable<Guest[]> {
    return this.http.get(`companies/${companyId}/guests/pending`);
  }

  public getCompanyReferents(companyId: number): Observable<Referent[]> {
    return this.http.get(`companies/${companyId}/referents`);
  }

  public getCompanyManualReferents(companyId: number): Observable<Referent[]> {
    return this.http.get(`companies/${companyId}/referents/manual`);
  }

  public getCompanyReferentById(companyId: number, referentId: number): Observable<Referent> {
    return this.http.get(`companies/${companyId}/referents/${referentId}`);
  }

  public registerReferent(newReferent: Referent, companyId: number): Observable<void> {
    return this.http.post(`companies/${companyId}/referents`, newReferent);
  }

  public updateReferent(referent: Referent, companyId: number, referentId: number): Observable<void> {
    return this.http.put(`companies/${companyId}/referents/${referentId}`, referent);
  }

  public registerUser(newUser: User, companyId: number): Observable<void> {
    return this.http.post(`companies/${companyId}/users`, newUser);
  }

  public registarGuestAuto(newGuest: Guest, companyId: number): Observable<void> {
    return this.http.post(`companies/${companyId}/guests/auto-ref`, newGuest);
  }

  public registerGuestManual(newGuest: Guest, companyId: number): Observable<void> {
    return this.http.post(`companies/${companyId}/guests/manual-ref`, newGuest);
  }

  public deleteConfirmedUser(
    companyId: number,
    userId: number
  ): Observable<void> {
    return this.http.delete(`companies/${companyId}/users/${userId}`);
  }

  public deleteReferent(companyId: number, referentId: number): Observable<void> {
    return this.http.delete(`companies/${companyId}/referents/${referentId}`);
  }

  public deletePendingUser(
    companyId: number,
    userId: number
  ): Observable<void> {
    return this.http.delete(`companies/${companyId}/pendingusers/${userId}`);
  }

  public exitCompanyGuest(companyId: number, guestId: number): Observable<void> {
    return this.http.put(`companies/${companyId}/guests/${guestId}`, null);
  }

  public blockCurrentUser(): Observable<void> {
    return this.http.put(`users/blockSelf`, null);
  }

  public getAllCompanies(): Observable<Company[]> {
    return this.http.get('companies');
  }

  public resetReferentPasswordOfCompany(companyId: number): Observable<void> {
    return this.http.post(`companies/${companyId}/referentnewpassword`, null);
  }

  public blockAllUsersOfCompany(companyId: number): Observable<void> {
    return this.http.post(`companies/${companyId}/block`, null);
  }

  public unblockAllUsersOfCompany(companyId: number): Observable<void> {
    return this.http.post(`companies/${companyId}/unblock`, null);
  }

  public deleteCompany(companyId: number): Observable<void> {
    return this.http.delete(`companies/${companyId}`);
  }

  public getBase64Logo(companyId: number): Observable<Image64> {
    return this.http.get<Image64>(`companies/${companyId}/base64logo`);
  }

  public uploadLogo(companyId: number, baseString: string): Observable<void> {
    return this.http.put(`companies/${companyId}/logo`, <Image64>{
      baseString: baseString
    });
  }

  public changeReferentMode(companyId: number): Observable<void> {
    return this.http.put(`companies/${companyId}/toggle-autocomplete-referent`, null);
  }

  public changeGuestMode(companyId: number): Observable<void> {
    return this.http.put(`companies/${companyId}/toggle-guest-exit-by-referent`, null);
  }

  public searchGuestsBySurname(companyId: number, surname: string): Observable<Guest[]> {
    return this.http.get<Guest[]>(`companies/${companyId}/guests/${surname}`);
  }

  public getDefaultReferent(companyId: number): Observable<Referent> {
    return this.http.get(`companies/${companyId}/referents/default`);
  }

}
