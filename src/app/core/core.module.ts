import { CommonModule, registerLocaleData } from '@angular/common';
import { NgModule, Optional, SkipSelf, LOCALE_ID } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import localeIt from '@angular/common/locales/it';
import localeEn from '@angular/common/locales/en';

import { BaseHttpService } from 'src/app/core/services/base-http.service';
import { ToastService } from 'src/app/core/services/toastr.service';
import { DeviceService } from 'src/app/core/services/device.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { NavigationService } from 'src/app/core/services/navigation.service';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import { IdentityService } from 'src/app/core/services/identity.service';
import { FileService } from 'src/app/core/services/file.service';
import { UserService } from 'src/app/core/services/user.service';
import { CompanyService } from 'src/app/core/services/company.service';
import { Language } from 'src/app/shared/models/language.model';
import { BlockService } from 'src/app/core/services/block.service';
import { TodayGuestService } from 'src/app/core/services/today-guest.service';
import { ContactService } from 'src/app/core/services/contact.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        deps: [HttpClient],
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory
      }
    })
  ],
  exports: [],
  declarations: [],
  providers: [
    BlockService,
    ToastService,
    DeviceService,
    SettingsService,
    NavigationService,
    LocalStorageService,
    BaseHttpService,
    IdentityService,
    FileService,
    UserService,
    CompanyService,
    TodayGuestService,
    ContactService,
    {
      provide: LOCALE_ID,
      useFactory: (translate: TranslateService) => {
        return translate.currentLang;
      },
      deps: [TranslateService]
    }
  ]
})
export class CoreModule {
  private readonly languages: Language[] = [
    { locale: 'it', label: 'Italiano', default: false },
    { locale: 'en', label: 'English', default: true }
    // TODO - Change locale and file to 'en-GB'
  ];

  public constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule,
    private translateSvc: TranslateService,
    private settingsSvc: SettingsService
  ) {
    if (parentModule) {
      throw new Error(
        'CoreModule has already been loaded. Import Core modules in the AppModule only.'
      );
    }

    registerLocaleData(localeIt, 'it');
    registerLocaleData(localeEn, 'en');

    this.translateSvc.addLangs(this.languages.map(l => l.locale));
    this.translateSvc.setDefaultLang(
      this.languages.find(l => l.default).locale
    );

    const browserLang = this.translateSvc.getBrowserLang();
    const settingsLang = this.settingsSvc.language;
    const regex = new RegExp(this.languages.map(l => l.locale).join('|'));

    if (settingsLang) {
      this.translateSvc.use(
        settingsLang.match(regex) ? settingsLang : this.translateSvc.defaultLang
      );
    } else {
      this.translateSvc.use(
        browserLang.match(regex) ? browserLang : this.translateSvc.defaultLang
      );
    }
  }
}

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, 'assets/i18n/', '.json');
}
