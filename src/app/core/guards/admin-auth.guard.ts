import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

import { IdentityService } from 'src/app/core/services/identity.service';
import { NavigationService } from 'src/app/core/services/navigation.service';
import { Roles } from 'src/app/shared/ui.utils';

@Injectable({
  providedIn: 'root'
})

export class AdminAuthGuard implements CanActivate {
  public constructor(
    private identitySvc: IdentityService,
    private navigationSvc: NavigationService
  ) { }

  public canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.identitySvc.currentIdentityEvt.value === null || this.identitySvc.currentIdentityEvt.value === undefined) {
      return false;
    }
    const isAdminOrAbove = this.identitySvc.currentIdentityEvt.value.role >= Roles.admin;
    return of(isAdminOrAbove).pipe(
      tap(() => {
        if (!isAdminOrAbove) {
          this.navigationSvc.home();
        }
      }
      )
    );
  }
}
