import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { IdentityService } from 'src/app/core/services/identity.service';
import { NavigationService } from 'src/app/core/services/navigation.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  public constructor(
    private identitySvc: IdentityService,
    private navigationSvc: NavigationService  ) {}

  public canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.identitySvc.isTokenValid().pipe(
      tap(isTokenValid => {
        if (!isTokenValid) {
          this.identitySvc.logout();
          this.navigationSvc.signIn();
        }
      })
    );
  }
}
