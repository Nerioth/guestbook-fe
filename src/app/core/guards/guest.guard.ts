import {CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate} from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';

import { GuestbookComponent } from 'src/app/home/guestbook/guestbook.component';
import { BlockService } from 'src/app/core/services/block.service';
import { NavigationService } from 'src/app/core/services/navigation.service';

@Injectable({
  providedIn: 'root'
})
export class GuestAuthGuard implements CanActivate, CanDeactivate<GuestbookComponent> {

  public constructor(private blockSvc: BlockService, private navigationSvc: NavigationService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): boolean | Observable<boolean> | Promise<boolean> {
      return this.blockSvc.isBlocked()
      .pipe(
        tap(block => {
          if (block) {
            this.navigationSvc.guestbook();
          }
        }),
        map(block => !block)
      );
  }

  public canDeactivate(
    component: GuestbookComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.blockSvc.isBlocked()
    .pipe(
      map(block => !block)
    );
  }
}
